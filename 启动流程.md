### 注册中心zookeeper
- 目录: cd /usr/local/apps/zookeeper-3.4.12/bin/
- 启动: ./zkServer.sh start, 重启:restart
- 状态: ps -aux | grep zookeeper

### RPC: dubbo管理项目
- 将dubbo部署到tomcat/webaps/
- 启动tomcat
    - cd /usr/local/apps/apache-tomcat-7.0.92
    - ./bin/startup.sh
    - 日志: tail -f ../logs/catalina.out
    - 状态: ps -aux | grep tomcat
- [管理](192.168.232.131:8080/dubbo)  
    
### dubbo监控服务
- 路径: cd /usr/local/apps/dubbo-monitor-simple-2.5.3    
- 启动: bin/server.sh start
- [查看](192.168.232.131:8088)
    
### DFS: FastDFS, 存储图片等文件
- 启动跟踪服务: /usr/bin/fdfs_trackerd /etc/fdfs/tracker.conf
- 启动储存服务: /usr/bin/fdfs_storaged /etc/fdfs/storage.conf

### Nginx, 用于跟FastDFS交互
- 目录: cd /usr/local/nginx/
- 启动: sbin/nginx<Enter>

### 商品搜索索引库: Solr
- 安装solr,配置
- 部署solr到tomcat
- 启动tomcat
- [管理](http://192.168.232.131:8080/solr)

### 消息中间件: RabbitMQ
- 位置: cd /usr/local/apps/rabbitmq_server-3.6.1/
- 启动: sbin/rabbitmq-server -detached
- [管理](http://192.168.232.131:15672)

### Redis
- 位置: cd /usr/local/redis3/bin/   
- 启动: ./redis-server ./redis.conf
    

### 启动服务工程
    - windows: springboot
    - linux: java jar xx-service-xx.jar
    
    
### 其它消费者工程