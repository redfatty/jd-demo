package com.hj.jd.timer.springtimer;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

//如果需要项目启动时就执行定时任务,就放到容器中去
@Component
public class ScheduledTask {

	@Scheduled(cron = "0/1 * * * * ? ")
	public void run() {
		Date date = new Date();
		System.out.println(Thread.currentThread().getName() + date);
	}

}
