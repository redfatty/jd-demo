package com.hj.jd.timer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class JdTimerDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(JdTimerDemoApplication.class, args);
	}

}

