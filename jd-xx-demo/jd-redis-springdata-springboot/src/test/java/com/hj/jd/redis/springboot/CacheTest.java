package com.hj.jd.redis.springboot;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.nio.channels.Pipe;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CacheTest {

	@Autowired
	private RedisTemplate redisTemplate;

	@Test
	public void test() {
		System.out.println("测试启动");
		Object v1 = redisTemplate.opsForValue().get("k1");
		System.out.println(v1);

		redisTemplate.opsForValue().set("username", "zs");

	}
}
