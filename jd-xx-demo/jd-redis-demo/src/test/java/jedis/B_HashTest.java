package jedis;

import org.junit.Test;
import redis.clients.jedis.Jedis;

import javax.sound.midi.Soundbank;
import java.util.*;

public class B_HashTest {

	@Test
	public void hashSetGetTest() {
		Jedis jedis = new Jedis("192.168.232.131", 6379);
		jedis.auth("hj");

		/**
		 * 一次添加一个字段
		 */
		jedis.hset("book:1", "name", "三国演义");
		jedis.hset("book:1", "price", "99.9");
		jedis.hset("book:1", "author", "罗贯中");

		/**
		 * 一次获取一个字段
		 */
		System.out.println("书名: " + jedis.hget("book:1", "name"));

		/**
		 * 获取全部字段
		 */
		Map<String, String> bookInfo = jedis.hgetAll("book:1");
		System.out.println("书籍信息: " + bookInfo);

		/**
		 * 长度
		 */
		Long len = jedis.hlen("book");
		System.out.println("有几本书: " + len);

		/**
		 * 字段是否存在
		 */
		boolean exists = jedis.hexists("book:1", "name");
		System.out.println(exists);

		/**
		 * 删除可变个数的字段
		 */
		jedis.hdel("book:1", "name", "price");
	}

	/**
	 * Multi操作
	 */
	@Test
	public void hashMultiOperateTest() {
		Jedis jedis = new Jedis("192.168.232.131", 6379);
		jedis.auth("hj");

		Map<String, String> map = new HashMap<>(3);
		map.put("name", "红楼梦");
		map.put("price", "108");
		map.put("author", "xx");

		/**
		 * 一次保存多个字段
		 */
		jedis.hmset("book:1", map);

		/**
		 * 获取多个字段
		 */
		List<String> list = jedis.hmget("book:1", "name", "price");
		System.out.println(list);
	}

}
