package jedis;

import org.junit.Test;
import org.springframework.test.web.client.UnorderedRequestExpectationManager;
import redis.clients.jedis.Jedis;

import java.util.Set;

public class E_SetTest {

	@Test
	public void hellSetTest() {
		Jedis jedis = JedisFactory.getJedis();

		//添加一个set
		jedis.sadd("set1", "a", "a", "b", "c");

		//获取所有成员
		Set<String> set1 = jedis.smembers("set1");
		System.out.println(set1);

		//移除
		jedis.srem("set1", "a");

		//是否包含
		jedis.sismember("set1", "c");

		//随机取出一个
		String string = jedis.spop("set1");


	}

	@Test
	public void calculateTest() {
		Jedis jedis = JedisFactory.getJedis();
		jedis.sadd("user:1:loves", "music", "game", "movie");
		jedis.sadd("user:2:loves", "trafic", "sprot", "music");

		//交集
		Set<String> inter = jedis.sinter("user:1:loves", "user:2:loves");
		System.out.println(inter); // music

		//并集
		Set<String> union = jedis.sunion("user:1:loves", "user:2:loves");
		System.out.println(union);

		//差集
		Set<String> diff = jedis.sdiff("user:1:loves", "user:2:loves");
		System.out.println(diff);
	}

}
