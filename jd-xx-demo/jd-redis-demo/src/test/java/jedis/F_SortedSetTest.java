package jedis;

import org.junit.Test;
import redis.clients.jedis.Jedis;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class F_SortedSetTest {

	@Test
	public void hellSortedSetTest() {
		Jedis jedis = JedisFactory.getJedis();

		//添加一个成员
		jedis.zadd("hotRank", 100.0, "Java");

		Map<String, Double> members = new HashMap<>(3);
		members.put("C++", 95.0);
		members.put("Objective-C", 90.0);
		members.put("Python", 88.0);

		//一次加多个成员
		jedis.zadd("hotRank", members);

		//求成员个数
		Long count = jedis.zcard("hotRank");
		System.out.println(count);

		//加分数
		jedis.zincrby("hotRank", 20, "Objectve-C");

		//求某个成员的分数
		Double score = jedis.zscore("hotRank", "Objective-C");
		System.out.println(score);

		//求某个成员的排名
		Long rank = jedis.zrank("hotRank", "C++");
		System.out.println(rank);

		//求指定范围内的成员
		Set<String> someMembers = jedis.zrange("hotRank", 0, -1);
		System.out.println(someMembers);

		//删除成员
		jedis.zrem("hotRank", "Python");
	}
}
