package jedis;

import org.junit.Test;
import redis.clients.jedis.Jedis;

import java.util.List;

public class C_ListTest {

	@Test
	public void listAsStackTest() {
		Jedis jedis = new Jedis("192.168.232.131", 6379);
		jedis.auth("hj");

		/**
		 * 默认往左边放
		 */
		jedis.lpush("nums", "1", "2", "3"); // 3, 2, 1

		/**
		 * 指定往右边放
		 */
		jedis.rpush("nums", "a", "b", "c"); // 3, 2, 1, a, b, c

		/**
		 * 从左边弹出
		 */
		jedis.lpop("nums");

		/**
		 * 从右弹出
		 */
		jedis.rpop("nums");

		/**
		 * 修改
		 */
		jedis.lset("nums", 0, "A");

		/**
		 * 求长度
		 */
		jedis.llen("nums");

		/**
		 * 取值
		 */
		jedis.lindex("nums", 0);

		/**
		 * 裁剪
		 */
		jedis.ltrim("nums", 0, -1);

		/**
		 * 按区间范围取
		 * 0: 最先存放的位置
		 * -1: 最后存放的位置
		 */
		List<String> nums = jedis.lrange("nums", 0, -1);
		System.out.println(nums);

		/**
		 * 删除
		 */
		jedis.del("nums");
	}
}
