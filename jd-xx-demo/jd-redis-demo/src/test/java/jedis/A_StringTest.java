package jedis;

import org.junit.Test;
import redis.clients.jedis.Jedis;

import javax.swing.plaf.synth.SynthOptionPaneUI;
import java.util.List;

public class A_StringTest {

	/**
	 * set get
	 */
	@Test
	public void setGetDelTest() {
		Jedis jedis = new Jedis("192.168.232.131", 6379);
		jedis.auth("hj");

		// set username 小明
		// get username

		jedis.set("username", "小明");
		System.out.println(jedis.get("username"));
		jedis.del("username");


	}


	/**
	 * Multi
	 */
	@Test
	public void multiSetGetDelTest() {
		Jedis jedis = JedisFactory.getJedis();

		// mset username hj, age 18
		// mget username age
		// del username age

		jedis.mset("username", "hj", "age", "18");
		List<String> list = jedis.mget("username", "age");
		System.out.println(list);
		jedis.del("username", "age");

	}

	/**
	 * 数值增加减少
	 */
	@Test
	public void incrementDecrementTest() {
		Jedis jedis = new Jedis("192.168.232.131", 6379);
		jedis.auth("hj");

		// set money 0, 初始为0
		// incr money, 加1
		// incrby money 100, 加100
		// decr money, 减1
		// decrby money 100, 减100
		// get money

		jedis.set("money", "0");
		System.out.println(jedis.get("money"));

		jedis.incr("money");
		System.out.println(jedis.get("money"));


		jedis.incrBy("money", 100);
		System.out.println(jedis.get("money"));

		jedis.decr("money");
		System.out.println(jedis.get("money"));

		jedis.decrBy("money", 100);
		System.out.println(jedis.get("money"));

		jedis.del("money");
	}


}
