package jedis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class JedisFactory {
	private final static String SERVER_HOST = "192.168.232.131";
	private final static int SERVER_PORT = 6379;
	private final static String PASSWORD = "hj";
	private final static int CONNECT_TIMEOUT = 2000;
	private static JedisPool jedisPool;

	static {
		JedisPoolConfig config = new JedisPoolConfig();
		config.setMaxIdle(50); //最长闲置时间
		config.setMaxTotal(100);//最大连接数
		config.setMaxWaitMillis(20000);//最长等待时间(毫秒)
		jedisPool = new JedisPool(config, SERVER_HOST, SERVER_PORT, CONNECT_TIMEOUT, PASSWORD);
	}

	public static Jedis getJedis() {
		return jedisPool.getResource();
	}
}
