package com.hj.rabbitmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JdRabbitmqSpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(JdRabbitmqSpringbootApplication.class, args);
	}

}

