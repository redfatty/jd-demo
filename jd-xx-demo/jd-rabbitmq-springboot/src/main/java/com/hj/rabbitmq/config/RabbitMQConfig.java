package com.hj.rabbitmq.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.xml.ws.WebEndpoint;

@Configuration
public class RabbitMQConfig {

	private final static String QUEUE_NAME = "product_queue_test";
	private final static String EXCHANGE_FANOUT = "fanout_exchange_test";
	private final static String EXCHANGE_Topic = "topic_exchange_test";

	@Bean
	public Queue getQueue() {
		return new Queue(QUEUE_NAME, true, false, false);
	}

	@Bean
	public FanoutExchange getFanoutExchange() {
		return new FanoutExchange(EXCHANGE_FANOUT);
	}

	@Bean
	public TopicExchange getTopicExchange() {
		return new TopicExchange(EXCHANGE_Topic);
	}

	@Bean
	public Binding getFanoutExchangeBinding(Queue queue, FanoutExchange fanoutExchange) {
		return BindingBuilder.bind(queue).to(fanoutExchange);
	}

	@Bean
	public Binding getTopicExchangeBinding(Queue queue, TopicExchange topicExchange) {
		return BindingBuilder.bind(queue).to(topicExchange).with("product.#");
	}

}
