package com.hj.rabbitmq.Client;

import com.hj.rabbitmq.entity.Product;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class Receiver {

//	@RabbitListener(queues = {"product_queue_test"})
//	public void processMsg(String msg) {
//		System.out.println("接收到消息: " + msg);
//	}

	@RabbitListener(queues = {"product_queue_test"})
	public void processProductMsg(Product product) {
		System.out.println("接收到消息: " + product);
	}

}
