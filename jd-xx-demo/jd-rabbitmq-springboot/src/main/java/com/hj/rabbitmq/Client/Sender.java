package com.hj.rabbitmq.Client;

import com.hj.rabbitmq.entity.Product;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Sender {

	@Autowired
	private RabbitTemplate rabbitTemplate;

	public void sendMsg(String msg) {
		//发送到队列
		rabbitTemplate.convertAndSend("product_queue_test", msg);
	}

	public void sendMsg2(String msg) {
		//发送到交换机
		rabbitTemplate.convertAndSend("fanout_exchange_test", "", msg);
	}

	public void sendProductMsg(Product product) {
		//发送到交换机
		rabbitTemplate.convertAndSend("topic_exchange_test", "product.add", product);
	}

}
