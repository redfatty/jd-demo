package com.hj.rabbitmq;

import com.hj.rabbitmq.Client.Sender;
import com.hj.rabbitmq.entity.Product;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JdRabbitmqSpringbootApplicationTests {

	@Autowired
	private Sender sender;

	@Test
	public void contextLoads() {
//		sender.sendMsg("SpringBoot整合RabbitMQ成功~");
//		sender.sendMsg2("发送到交换机的消息~");

		sender.sendProductMsg(new Product(1L, "手机", 1000L));
	}
}

