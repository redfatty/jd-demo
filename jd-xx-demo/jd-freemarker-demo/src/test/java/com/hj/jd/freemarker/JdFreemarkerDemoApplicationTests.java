package com.hj.jd.freemarker;

import com.hj.jd.freemarker.entity.User;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JdFreemarkerDemoApplicationTests {

	@Autowired
	private Configuration configuration;

	@Test
	public void contextLoads() {
		System.out.println(System.getProperty("user.dir"));
	}

	@Test
	public void createHtmlTest() {
		try {
			//1.构建一个模板对象
			Template template = configuration.getTemplate("hello.ftl");

			//2.构建数据
			Map<String, Object> datas = new HashMap<>();
			//基本类型
			datas.put("money", 10000);
			//字符串
			datas.put("msg", "Hello Freemarker!");
			//空对象
			datas.put("xx", null);
			//对象
			datas.put("user", new User(1L, "小明", new Date()));
			//集合
			List<User> userList = new ArrayList<>(5);
			datas.put("userList", userList);
			for (int i = 0; i < 5; i++) {
				userList.add(new User(i + 1L, "小明_" + i, new Date()));
			}

			//加工模板,输出到一个html文件中
			FileWriter writer = new FileWriter(System.getProperty("user.dir") + File.separator + "hello.html");
			template.process(datas, writer);
			System.out.println("模板加工成功");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TemplateException e) {
			e.printStackTrace();
		}
	}

}

