package com.hj.jd.freemarker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JdFreemarkerDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(JdFreemarkerDemoApplication.class, args);
	}

}

