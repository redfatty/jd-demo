<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>

<#--字符串-->
${msg}

<#--空对象处理-->
    空对象:${xx!}
    空对象加默认值:${xx!'null默认值'}

<#--对象-->
${user.id}
${user.username}
<#--时间-->
${user.birthday?date}
${user.birthday?time}
${user.birthday?datetime}
<#--${user.birthday?'yyyy-MM-dd HH:mm:ss'}-->

<#--if-->
<#if (money > 1000000)>
    土豪
</#if>

<#--if elseif else-->
<#if (money > 1000000)>
    土豪
<#elseif (money > 10000)>
    小康
<#else >
    要饭的
</#if>

<#--集合遍历-->
<#list userList as user>
    id:       ${user.id}
    username: ${user.username}
    birthday: ${user.birthday?date}
</#list>

</body>
</html>