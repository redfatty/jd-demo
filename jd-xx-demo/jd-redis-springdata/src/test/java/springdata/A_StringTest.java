package springdata;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-redis.xml")
public class A_StringTest {

	@Autowired
	private RedisTemplate redisTemplate;

	@Test
	public void test() {
		Object v1 = redisTemplate.opsForValue().get("k1");
		System.out.println(v1);
	}
}
