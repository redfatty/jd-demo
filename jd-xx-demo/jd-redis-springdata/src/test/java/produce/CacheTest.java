package produce;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import springdata.ProductType;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-redis.xml")
public class CacheTest {

	@Autowired
	private RedisTemplate redisTemplate;

	@Test
	public void queryProductTypeByIdTest() {
		Long id = 1L;
		String typeKey = "product:type:" + id;
		ProductType type = (ProductType) redisTemplate.opsForValue().get(typeKey);
		if (type == null) {
			String uuid = UUID.randomUUID().toString();
			String lockKey = "product:type:" + id + ":lock";
			Boolean lockSuccess = redisTemplate.opsForValue().setIfAbsent(lockKey, uuid);
			if (lockSuccess) {
				//要将锁释放
				//第一种,设置有效期
//				redisTemplate.expire("product:type:lock", 100, TimeUnit.MILLISECONDS);

				//第二种, 查询业务在try中进行, finally中手动释放锁
				try {
					System.out.println("从数据库中查询~");
					//从数据库中查
					ProductType typeFromDB = new ProductType(id, "手机数码");
					//保存至redis
					redisTemplate.opsForValue().set(typeKey, typeFromDB);
				} finally {
					//解除锁
					if (uuid.equals(redisTemplate.opsForValue().get(lockKey))) {
						redisTemplate.delete(lockKey);
					}
				}
			} else {
				//其它线程正在查询, 先等待100毫秒
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				//100毫秒之后再查询
				queryProductTypeByIdTest();
			}
		} else {
			System.out.println("使用redis缓存中的数据~");
		}
	}

	/**
	 * 有问题
	 */
	@Test
	public void throughCacheQueryTest() {
		Long id = 1L;
		String typeKey = "product:type:" + id;
		ProductType type = (ProductType) redisTemplate.opsForValue().get(typeKey);
		if (type == null) {
			System.out.println("从数据库中查询~");
			//从数据库中查
			ProductType typeFromDB = new ProductType(id, "手机数码");
			//保存至redis
			redisTemplate.opsForValue().set(typeKey, typeFromDB);
		} else {
			System.out.println("使用redis缓存中的数据~");
		}
	}

	/**
	 * 更新数据库
	 */
	@Test
	public void updateProductTypeTest() {
		//更新数据的正确处理方式
		//1.更新数据库
		//productTypeService.update(productType);

		//2.删除redis缓存
//		redisTemplate.delete("product:type:1");

		//3.用户下次再查询时,再去存入redis缓存中
	}

	/**
	 * 并发访问
	 * 查询商品类别信息
	 */
	@Test
	public void concurrentAccessTest() {
		deleteAllDatas();
		redisTemplate.delete("product:type:lock");

		ExecutorService threadPool = new ThreadPoolExecutor(
				100,
				200,
				100L,
				TimeUnit.MILLISECONDS,
				new LinkedBlockingQueue<Runnable>(100));

		List<Future<Boolean>> futureList = new ArrayList<>();

		for (int i = 0; i < 100; i++) {
			Callable<Boolean> callable = new Callable<Boolean>() {
				@Override
				public Boolean call() throws Exception {
					throughCacheQueryTest();
					return true;
				}
			};

			Future<Boolean> future = threadPool.submit(callable);
			futureList.add(future);
			//如果在这里调用future的get方法, 将会立即执行该任务, 多个任务将会串行执行
			//Boolean retVal = future.get();

//			Runnable runnable = new Runnable() {
//				@Override
//				public void run() {
//					queryProductTypeByIdTest();
//				}
//			};
//			threadPool.submit(runnable);
		}

		//在所有任务都提交后, 再来遍历所有的future,并调用get方法, 可以实现并发的效果
		for (Future<Boolean> future : futureList) {
			try {
				Boolean retVal = future.get();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}

		//		try {
		//			Thread.sleep(1000000);
		//		} catch (InterruptedException e) {
		//			e.printStackTrace();
		//		}
	}

	private void initDatas() {
		deleteAllDatas();

		redisTemplate.opsForValue().set("product:type:1", new ProductType(1L, "手机数码"));
		redisTemplate.opsForValue().set("product:type:2", new ProductType(2L, "家用电器"));
		redisTemplate.opsForValue().set("product:type:3", new ProductType(3L, "服装"));
	}

	private void deleteAllDatas() {
		redisTemplate.delete("product:type:1");
		redisTemplate.delete("product:type:2");
		redisTemplate.delete("product:type:3");
	}

	private ProductType getProductTypeFromDBById(Long productTypeId) {
		return null;
	}
}
