package com.hj.wechat.util;

import com.hj.wechat.constant.WxMsgEleName;
import com.hj.wechat.constant.WxMsgType;
import com.hj.wechat.entity.*;
import com.thoughtworks.xstream.XStream;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import java.util.Date;
import java.util.List;

public class WxMsgUtils {

	private WxMsgUtils() {
	}

	public static WxBaseMsg fromXml(String msgXml) {
		WxBaseMsg wxMsg = null;
		Document document = null;
		try {
			document = DocumentHelper.parseText(msgXml);
			Element rootEle = document.getRootElement();
			Element msgTypeEle = rootEle.element(WxMsgEleName.MSG_TYPE);
			if (msgTypeEle == null) {
				return WxBaseMsg.unknownMsg();
			}

			//不同类型消息的特定信息
			String msgType = msgTypeEle.getText();
			if (WxMsgType.TEXT.equals(msgType)) {
				//文本消息
				wxMsg = new WxTextMsg(getText(rootEle, WxMsgEleName.CONTENT));
			} else if (WxMsgType.IMAGE.equals(msgType)) {
				//图片消息
				wxMsg = new WxImageMsg(
						getText(rootEle, WxMsgEleName.MEDIA_ID),
						getText(rootEle, WxMsgEleName.PIC_URL));
			} else if (WxMsgType.VOICE.equals(msgType)) {
				//语音消息
				wxMsg = new WxVoiceMsg(
						getText(rootEle, WxMsgEleName.MEDIA_ID),
						getText(rootEle, WxMsgEleName.FORMAT));
			} else if (WxMsgType.VIDEO.equals(msgType)) {
				//视频消息
				wxMsg = new WxVideoMsg(
						getText(rootEle, WxMsgEleName.MEDIA_ID),
						getText(rootEle, WxMsgEleName.THUMB_MEDIA_ID));
			} else if (WxMsgType.SHORTVIDEO.equals(msgType)) {
				//短视频消息
				wxMsg = new WxShortVideoMsg(
						getText(rootEle, WxMsgEleName.MEDIA_ID),
						getText(rootEle, WxMsgEleName.THUMB_MEDIA_ID));
			} else if (WxMsgType.LINK.equals(msgType)) {
				//链接消息
				wxMsg = new WxLinkMsg(
						getText(rootEle, WxMsgEleName.TITLE),
						getText(rootEle, WxMsgEleName.DESCRIPTION),
						getText(rootEle, WxMsgEleName.URL));
			} else if (WxMsgType.LOCATION.equals(msgType)) {
				wxMsg = new WxLocationMsg(
						getText(rootEle, WxMsgEleName.LOCATION_X),
						getText(rootEle, WxMsgEleName.LOCATION_Y),
						getText(rootEle, WxMsgEleName.SCALE),
						getText(rootEle, WxMsgEleName.LABEL));
			} else if (WxMsgType.EVENT.equals(msgType)) {
				wxMsg = new WxEventMsg(getText(rootEle, WxMsgEleName.EVENT));
			} else {
				wxMsg = WxBaseMsg.unknownMsg();
			}

			//消息基本信息
			wxMsg.setMsgId(getText(rootEle, WxMsgEleName.MSG_ID));
			wxMsg.setToUserName(getText(rootEle, WxMsgEleName.TO_USER_NAME));
			wxMsg.setFromUserName(getText(rootEle, WxMsgEleName.FROM_USER_NAME));
			wxMsg.setCreateTime(getText(rootEle, WxMsgEleName.CREATE_TIME));
		} catch (DocumentException e) {
			e.printStackTrace();
			wxMsg = WxBaseMsg.unknownMsg();
		}
		return wxMsg;
	}

	public static String toXml(WxBaseMsg wxMsg) {
		XStream xStream = new XStream();
		//指定别名,否则是类型名
		xStream.alias("xml", wxMsg.getClass());
		return xStream.toXML(wxMsg);
	}

	public static String createTextMsgXml(String fromUser, String toUser, String content) {
		WxTextMsg textMsg = new WxTextMsg(content);
		textMsg.setMsgType(WxMsgType.TEXT);
		textMsg.setFromUserName(fromUser);
		textMsg.setToUserName(toUser);
		textMsg.setCreateTime(new Date().getTime() + "");
		return toXml(textMsg);
	}

	private static String getText(Element rootEle, String subEleName) {
		Element subEle = rootEle.element(subEleName);
		if (subEle != null) {
			return subEle.getText();
		}
		return null;
	}
}
