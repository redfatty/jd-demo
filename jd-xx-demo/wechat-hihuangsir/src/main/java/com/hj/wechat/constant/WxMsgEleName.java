package com.hj.wechat.constant;

import org.dom4j.Element;

public interface WxMsgEleName {
	String MSG_ID = "MsgId";
	String TO_USER_NAME = "ToUserName";
	String FROM_USER_NAME = "FromUserName";
	String CREATE_TIME = "CreateTime";
	String MSG_TYPE = "MsgType";

	//文本
	String CONTENT = "Content";

	//多媒体的
	String MEDIA_ID = "MediaId";

	//图片
	String PIC_URL = "PicUrl";

	//语音
	String FORMAT = "Format";

	//视频,短视频
	String THUMB_MEDIA_ID = "ThumbMediaId";

	//链接
	String TITLE = "Title";
	String DESCRIPTION = "Description";
	String URL = "Url";

	//位置
	String LOCATION_X = "Location_X";
	String LOCATION_Y = "Location_Y";
	String SCALE = "Scale";
	String LABEL = "Label";

	//推送事件
	String EVENT = "Event";
}


