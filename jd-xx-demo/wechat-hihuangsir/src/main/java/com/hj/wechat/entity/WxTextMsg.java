package com.hj.wechat.entity;

import com.hj.wechat.constant.WxMsgType;

import java.io.Serializable;

public class WxTextMsg extends WxBaseMsg implements Serializable {

	private String Content;

	public WxTextMsg() {
	}

	public WxTextMsg(String content) {
		Content = content;
		MsgType = WxMsgType.TEXT;
	}

	public String getContent() {
		return Content;
	}

	public void setContent(String content) {
		Content = content;
	}

	@Override
	public String toString() {
		return "WxTextMsg{" +
				"Content='" + Content + '\'' +
				", MsgId='" + MsgId + '\'' +
				", FromUserName='" + FromUserName + '\'' +
				", ToUserName='" + ToUserName + '\'' +
				", CreateTime='" + CreateTime + '\'' +
				", MsgType='" + MsgType + '\'' +
				'}';
	}
}
