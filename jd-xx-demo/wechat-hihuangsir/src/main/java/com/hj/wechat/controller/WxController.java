package com.hj.wechat.controller;

import com.hj.wechat.constant.WxConst;
import com.hj.wechat.constant.WxMsgType;
import com.hj.wechat.entity.WxBaseMsg;
import com.hj.wechat.entity.WxEventMsg;
import com.hj.wechat.entity.WxTextMsg;
import com.hj.wechat.util.CheckUtils;
import com.hj.wechat.util.SHA1Utils;
import com.hj.wechat.util.WxMsgUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.TreeSet;

@Controller
public class WxController {

	@GetMapping("/index")
	@ResponseBody
	public String index(HttpServletResponse response) {

		Cookie cookie = new Cookie("mycookie", "135fad*3423");
		cookie.setMaxAge(10000);
		cookie.setPath("/");
		response.addCookie(cookie);
		return "hello";
	}

	@GetMapping("/onWxMsg")
	@ResponseBody
	public String checkSignature(String signature, String timestamp, String nonce, String echostr) {
		//参数:signature, timestamp, nonce, echostr
		//1）将token、timestamp、nonce三个参数进行字典序排序
		//2）将三个参数字符串拼接成一个字符串进行sha1加密
		//3）开发者获得加密后的字符串可与signature对比，标识该请求来源于微信
		//4) 将echostr原样返回

		if (CheckUtils.checkSignature(signature, timestamp, nonce)) {
			return echostr;
		}
		return "Who Are You?";
	}

	@PostMapping("/onWxMsg")
	@ResponseBody
	public String onWxMsg(HttpServletRequest request, @RequestBody String rawMsg) {
		//图片:http://mmbiz.qpic.cn/mmbiz_jpg/nTDPGibmjlUT6h6icNC4WAK0y6SLbCtWdNvAxiaV6S1dNqWDDbiaicaYKw8rPC6nGzwdBdqEO6pp4UzpEI4dZib6FlRw/0
		//图片:http://mmbiz.qpic.cn/mmbiz_jpg/nTDPGibmjlUT6h6icNC4WAK0y6SLbCtWdNXaYsI1DEKZ0mxCSWKyOBp0dVcVyjT0CVH1AFzOIUZILqTUjvsiaMySA/0
		WxBaseMsg baseMsg = WxMsgUtils.fromXml(rawMsg);
		System.out.println("收到消息: " + baseMsg);
		String msgType = baseMsg.getMsgType();
		if (WxMsgType.UNKNOWN.equals(msgType)) {
			return WxMsgUtils.createTextMsgXml(baseMsg.getToUserName(), baseMsg.getFromUserName(), "\"你好~ 客服小姐姐正在路上, 请稍等~\"");
		} else if (WxMsgType.EVENT.equals(msgType)) {
			WxEventMsg eventMsg = (WxEventMsg) baseMsg;
			if (WxMsgType.EVENT_SUBSCRIBE.equals(eventMsg.getEvent())) {
				return WxMsgUtils.createTextMsgXml(baseMsg.getToUserName(), baseMsg.getFromUserName(), "谢谢关注!");
			} else {
				System.out.println("有用户取关啦~");
			}
		}

		return WxMsgUtils.createTextMsgXml(baseMsg.getToUserName(), baseMsg.getFromUserName(), "你好");
	}

}
