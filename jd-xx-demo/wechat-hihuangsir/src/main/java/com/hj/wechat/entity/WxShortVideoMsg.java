package com.hj.wechat.entity;

import com.hj.wechat.constant.WxMsgType;

import java.io.Serializable;

public class WxShortVideoMsg extends WxBaseMsg implements Serializable {
	private String MediaId;
	private String ThumbMediaId;

	public WxShortVideoMsg() {
	}

	public WxShortVideoMsg(String mediaId, String thumbMediaId) {
		MediaId = mediaId;
		ThumbMediaId = thumbMediaId;
		MsgType = WxMsgType.SHORTVIDEO;
	}

	public String getMediaId() {
		return MediaId;
	}

	public void setMediaId(String mediaId) {
		MediaId = mediaId;
	}

	public String getThumbMediaId() {
		return ThumbMediaId;
	}

	public void setThumbMediaId(String thumbMediaId) {
		ThumbMediaId = thumbMediaId;
	}

	@Override
	public String toString() {
		return "WxShortVideoMsg{" +
				"MediaId='" + MediaId + '\'' +
				", ThumbMediaId='" + ThumbMediaId + '\'' +
				", MsgId='" + MsgId + '\'' +
				", FromUserName='" + FromUserName + '\'' +
				", ToUserName='" + ToUserName + '\'' +
				", CreateTime='" + CreateTime + '\'' +
				", MsgType='" + MsgType + '\'' +
				'}';
	}
}
