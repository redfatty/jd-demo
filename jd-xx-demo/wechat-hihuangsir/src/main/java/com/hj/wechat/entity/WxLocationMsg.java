package com.hj.wechat.entity;

import com.hj.wechat.constant.WxMsgType;

import java.io.Serializable;

public class WxLocationMsg extends WxBaseMsg implements Serializable {
	private String Location_X;
	private String Location_Y;
	private String Scale;
	private String Label;

	public WxLocationMsg() {
	}

	public WxLocationMsg(String location_X, String location_Y, String scale, String label) {
		Location_X = location_X;
		Location_Y = location_Y;
		Scale = scale;
		Label = label;
		MsgType = WxMsgType.LOCATION;
	}

	public String getLocation_X() {
		return Location_X;
	}

	public void setLocation_X(String location_X) {
		Location_X = location_X;
	}

	public String getLocation_Y() {
		return Location_Y;
	}

	public void setLocation_Y(String location_Y) {
		Location_Y = location_Y;
	}

	public String getScale() {
		return Scale;
	}

	public void setScale(String scale) {
		Scale = scale;
	}

	public String getLabel() {
		return Label;
	}

	public void setLabel(String label) {
		Label = label;
	}

	@Override
	public String toString() {
		return "WxLocationMsg{" +
				"Location_X='" + Location_X + '\'' +
				", Location_Y='" + Location_Y + '\'' +
				", Scale='" + Scale + '\'' +
				", Label='" + Label + '\'' +
				", MsgId='" + MsgId + '\'' +
				", FromUserName='" + FromUserName + '\'' +
				", ToUserName='" + ToUserName + '\'' +
				", CreateTime='" + CreateTime + '\'' +
				", MsgType='" + MsgType + '\'' +
				'}';
	}
}
