package com.hj.wechat.entity;

import com.hj.wechat.constant.WxMsgType;
import org.dom4j.Element;

import java.io.Serializable;

public class WxBaseMsg implements Serializable {
	protected String MsgId;
	protected String FromUserName;
	protected String ToUserName;
	protected String CreateTime;
	protected String MsgType;

	public static WxBaseMsg unknownMsg() {
		WxBaseMsg baseMsg = new WxBaseMsg();
		baseMsg.setMsgType(WxMsgType.UNKNOWN);
		return baseMsg;
	}

	public String getMsgId() {
		return MsgId;
	}

	public void setMsgId(String msgId) {
		MsgId = msgId;
	}

	public String getFromUserName() {
		return FromUserName;
	}

	public void setFromUserName(String fromUserName) {
		FromUserName = fromUserName;
	}

	public String getToUserName() {
		return ToUserName;
	}

	public void setToUserName(String toUserName) {
		ToUserName = toUserName;
	}

	public String getCreateTime() {
		return CreateTime;
	}

	public void setCreateTime(String createTime) {
		CreateTime = createTime;
	}

	public String getMsgType() {
		return MsgType;
	}

	public void setMsgType(String msgType) {
		MsgType = msgType;
	}

	@Override
	public String toString() {
		return "WxBaseMsg{" +
				"MsgId='" + MsgId + '\'' +
				", FromUserName='" + FromUserName + '\'' +
				", ToUserName='" + ToUserName + '\'' +
				", CreateTime='" + CreateTime + '\'' +
				", MsgType='" + MsgType + '\'' +
				'}';
	}
}
