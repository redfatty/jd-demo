package com.hj.wechat.pojo;

import com.hj.wechat.constant.ButtonType;
import com.hj.wechat.constant.WxConst;

public class ViewButton extends BaseButton {
	public ViewButton() {
	}

	public ViewButton(String url) {
		this.url = url;
		setType(ButtonType.VIEW);
	}

	private String url;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
