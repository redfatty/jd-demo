package com.hj.wechat.util;

import com.google.gson.Gson;
import com.hj.wechat.constant.WxConst;
import com.hj.wechat.pojo.AccessToken;
import com.hj.wechat.pojo.BaseButton;
import com.hj.wechat.pojo.ClickButton;
import com.hj.wechat.pojo.ViewButton;
import net.sf.json.JSONObject;
import okhttp3.*;
import okio.BufferedSink;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

public class WxUtils {
	private WxUtils() {

	}

	public static AccessToken getAccessToken() {
		//先去redis中取
		//没有再去微信服务器取
		OkHttpClient httpClient = new OkHttpClient();
		Request request = new Request.Builder()
				.url(WxConst.ACCESS_TOKEN_URL)
				.get()
				.build();
		Call call = httpClient.newCall(request);
		Response response = null;
		try {
			//同步请求
			response = call.execute();
			//{"access_token":"ACCESS_TOKEN","expires_in":7200}
			//{"errcode":40013,"errmsg":"invalid appid"}
			String json = response.body().string();
			JSONObject jsonObject = JSONObject.fromObject(json);
			if (jsonObject != null) {
				if (jsonObject.getString("access_token") != null) {
					AccessToken accessToken = new AccessToken();
					accessToken.setAccess_token(jsonObject.getString("access_token"));
					accessToken.setExpires_in(jsonObject.getLong("expires_in"));
					return accessToken;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (response != null) {
				response.close();
			}
		}

		return null;
	}

	public static List<BaseButton> createButtons() {
		//今日歌曲
		//体验服务
			//加入会员
			//查看附近的店

		ClickButton btn1 = new ClickButton("V1001_TODAY_MUSIC");
		btn1.setName("今日推荐");

		BaseButton btn2 = new BaseButton();
		btn2.setName("体验服务");

		ViewButton btn21 = new ViewButton("");
		btn21.setName("加入会员");

		ViewButton btn22 = new ViewButton("");
		btn22.setName("查看附近的店");

		btn2.setSub_button(new ArrayList<BaseButton>(2));
		btn2.getSub_button().add(btn21);
		btn2.getSub_button().add(btn22);

		List<BaseButton> list = new ArrayList<>(2);
		list.add(btn1);
		list.add(btn2);

		return list;
	}

	public static void main(String[] args) throws IOException {
		List<BaseButton> buttons = createButtons();
		Map<String, List<BaseButton>> menu = new HashMap<>();
		menu.put("button", buttons);
		String menuJson = new Gson().toJson(menu);

		AccessToken accessToken = getAccessToken();
		String requestURL = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token="+accessToken.getAccess_token();

		URL url = new URL(requestURL);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("POST");
		connection.setDoOutput(true);
		connection.setDoInput(true);
		//需要将菜单的信息发送给微信服务器
		OutputStream outputStream = connection.getOutputStream();
		outputStream.write(menuJson.getBytes());
		//
		InputStream inputStream = connection.getInputStream();
		//{"access_token":"ACCESS_TOKEN","expires_in":7200}
		int len = 0;
		byte[] bs = new byte[1024];
		//内存流
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		while((len = inputStream.read(bs))!= -1){
			//
			byteArrayOutputStream.write(bs,0,len);
		}
		//
		String json = new String(byteArrayOutputStream.toByteArray());
		//
		System.out.println(json);
	}
}
