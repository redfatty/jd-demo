package com.hj.wechat.entity;

import com.hj.wechat.constant.WxMsgType;

public class WxEventMsg extends WxBaseMsg {
	private String Event;

	public WxEventMsg() {
	}

	public WxEventMsg(String event) {
		Event = event;
		MsgType = WxMsgType.EVENT;
	}

	public String getEvent() {
		return Event;
	}

	public void setEvent(String event) {
		Event = event;
	}

	@Override
	public String toString() {
		return "WxEventMsg{" +
				"Event='" + Event + '\'' +
				", MsgId='" + MsgId + '\'' +
				", FromUserName='" + FromUserName + '\'' +
				", ToUserName='" + ToUserName + '\'' +
				", CreateTime='" + CreateTime + '\'' +
				", MsgType='" + MsgType + '\'' +
				'}';
	}
}
