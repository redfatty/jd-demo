package com.hj.wechat.entity;

import com.hj.wechat.constant.WxMsgType;

import java.io.Serializable;

public class WxLinkMsg extends WxBaseMsg implements Serializable {
	private String Title;
	private String Description;
	private String Url;

	public WxLinkMsg() {
	}

	public WxLinkMsg(String title, String description, String url) {
		Title = title;
		Description = description;
		Url = url;
		MsgType = WxMsgType.LINK;
	}

	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getUrl() {
		return Url;
	}

	public void setUrl(String url) {
		Url = url;
	}

	@Override
	public String toString() {
		return "WxLinkMsg{" +
				"Title='" + Title + '\'' +
				", Description='" + Description + '\'' +
				", Url='" + Url + '\'' +
				", MsgId='" + MsgId + '\'' +
				", FromUserName='" + FromUserName + '\'' +
				", ToUserName='" + ToUserName + '\'' +
				", CreateTime='" + CreateTime + '\'' +
				", MsgType='" + MsgType + '\'' +
				'}';
	}
}
