package com.hj.wechat.util;

import com.hj.wechat.constant.WxConst;

import java.util.TreeSet;

public class CheckUtils {
	private CheckUtils() {
	}

	public static boolean checkSignature(String signature, String timestamp, String nonce) {
		TreeSet<String> treeSet = new TreeSet<>();
		treeSet.add(WxConst.TOKEN);
		treeSet.add(timestamp);
		treeSet.add(nonce);
		StringBuilder stringBuilder = new StringBuilder();
		for (String s : treeSet) {
			stringBuilder.append(s);
		}
		return signature.equals(SHA1Utils.encode(stringBuilder.toString()));
	}
}
