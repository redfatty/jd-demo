package com.hj.wechat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class WechatHihuangsirApplication {

	public static void main(String[] args) {
		SpringApplication.run(WechatHihuangsirApplication.class, args);
	}

}

