package com.hj.wechat.entity;

import com.hj.wechat.constant.WxMsgType;
import org.dom4j.Element;

import java.io.Serializable;

public class WxVoiceMsg extends WxBaseMsg implements Serializable {
	private String MediaId;
	private String Format;

	public WxVoiceMsg() {
	}

	public WxVoiceMsg(String mediaId, String format) {
		MediaId = mediaId;
		Format = format;
		MsgType = WxMsgType.VOICE;
	}

	public String getMediaId() {
		return MediaId;
	}

	public void setMediaId(String mediaId) {
		MediaId = mediaId;
	}

	public String getFormat() {
		return Format;
	}

	public void setFormat(String format) {
		Format = format;
	}

	@Override
	public String toString() {
		return "WxVoiceMsg{" +
				"MediaId='" + MediaId + '\'' +
				", Format='" + Format + '\'' +
				", MsgId='" + MsgId + '\'' +
				", FromUserName='" + FromUserName + '\'' +
				", ToUserName='" + ToUserName + '\'' +
				", CreateTime='" + CreateTime + '\'' +
				", MsgType='" + MsgType + '\'' +
				'}';
	}
}
