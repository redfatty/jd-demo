package com.hj.wechat.entity;

import com.hj.wechat.constant.WxMsgType;
import org.dom4j.Element;

import java.io.Serializable;

public class WxImageMsg extends WxBaseMsg implements Serializable {
	private String MediaId;
	private String PicUrl;

	public WxImageMsg() {
	}

	public WxImageMsg(String mediaId, String picUrl) {
		MediaId = mediaId;
		PicUrl = picUrl;
		MsgType = WxMsgType.IMAGE;
	}

	public String getMediaId() {
		return MediaId;
	}

	public void setMediaId(String mediaId) {
		MediaId = mediaId;
	}

	public String getPicUrl() {
		return PicUrl;
	}

	public void setPicUrl(String picUrl) {
		PicUrl = picUrl;
	}

	@Override
	public String toString() {
		return "WxImageMsg{" +
				"MediaId='" + MediaId + '\'' +
				", PicUrl='" + PicUrl + '\'' +
				", MsgId='" + MsgId + '\'' +
				", FromUserName='" + FromUserName + '\'' +
				", ToUserName='" + ToUserName + '\'' +
				", CreateTime='" + CreateTime + '\'' +
				", MsgType='" + MsgType + '\'' +
				'}';
	}
}
