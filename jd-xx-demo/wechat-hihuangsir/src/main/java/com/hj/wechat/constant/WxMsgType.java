package com.hj.wechat.constant;

public interface WxMsgType {
	String UNKNOWN = "unknown";
	String TEXT = "text";
	String IMAGE = "image";
	String VOICE = "voice";
	String VIDEO = "video";
	String SHORTVIDEO = "shortvideo";
	String LOCATION = "location";
	String LINK = "link";
	String EVENT = "event";
	String EVENT_SUBSCRIBE = "subscribe";
	String EVENT_UNSUBSCRIBE = "unsubscribe";
}
