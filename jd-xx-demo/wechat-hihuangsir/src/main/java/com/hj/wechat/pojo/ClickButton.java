package com.hj.wechat.pojo;

import com.hj.wechat.constant.ButtonType;

public class ClickButton extends BaseButton {
	public ClickButton() {
	}

	public ClickButton(String key) {
		this.key = key;
		setType(ButtonType.CLICK);
	}

	private String key;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
}
