package com.hj.jd.email;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMailMessage;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.test.context.junit4.SpringRunner;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JdEmailDemoApplicationTests {

	private final static String EMAIL_FROM = "redfatty@163.com";
	private final static String EMAIL_TO = "hj08101@163.com";

	@Autowired
	private JavaMailSender mailSender;

	@Autowired
	private TemplateEngine templateEngine;

	@Test
	public void contextLoads() {
	}

	@Test
	public void sendSimpleEmailTest() {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setFrom(EMAIL_FROM);
		message.setTo(EMAIL_TO);
		message.setSubject("简单邮件");
		message.setText("SpringBoot整合JavaMail成功发送邮件");
		//发送
		mailSender.send(message);
	}

	@Test
	public void sendHrmlEmailTest() {
		MimeMessage message = mailSender.createMimeMessage();
		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setFrom(EMAIL_FROM);
			helper.setTo(EMAIL_TO);
			helper.setSubject("带超链接的邮件");
			//设置邮件内容,解析成html格式
			helper.setText("点击链接,完成激活<a href='http://www.baidu.com'> http://www.baidu.com </a>", true);
			mailSender.send(message);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void sendEmailWithAttachment() {
		MimeMessage message = mailSender.createMimeMessage();
		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setFrom(EMAIL_FROM);
			helper.setTo(EMAIL_TO);
			helper.setSubject("带附件的邮件");
			//设置邮件内容,解析成html格式
			helper.setText("点击链接,完成激活<a href='http://www.baidu.com'> http://www.baidu.com </a>", true);
			//添加附件
			String filePath = "C:\\Users\\huangjiong\\Desktop\\timg.jpg";
			FileSystemResource resource = new FileSystemResource(filePath);
			helper.addAttachment("timg.jpg", resource);
			//发送
			mailSender.send(message);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}


	@Test
	public void sendTemplateEmail() {
		MimeMessage message = mailSender.createMimeMessage();
		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setFrom(EMAIL_FROM);
			helper.setTo(EMAIL_TO);
			helper.setSubject("根据模板生成的邮件");
			//根据模板生成邮件内容
			Context context = new Context();
			context.setVariable("username", "小明");
			String content = templateEngine.process("email", context);
			helper.setText(content, true);
			//发送
			mailSender.send(message);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}


}

