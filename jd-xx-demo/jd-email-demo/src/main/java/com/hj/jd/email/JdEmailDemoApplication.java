package com.hj.jd.email;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JdEmailDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(JdEmailDemoApplication.class, args);
	}

}

