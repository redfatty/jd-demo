package com.hj.jd.pay.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JdPayDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(JdPayDemoApplication.class, args);
	}

}

