package com.hj.b_work;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Sender {
	private final static String QUEUE_NAME = "work";

	public static void main(String[] args) throws IOException, TimeoutException {

		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("192.168.232.131");
		factory.setPort(5672);
		factory.setVirtualHost("/hj");
		factory.setUsername("hj");
		factory.setPassword("hj");
		Connection connection = factory.newConnection();

		Channel channel = connection.createChannel();
		channel.queueDeclare(QUEUE_NAME, false, false, false, null);

		for (int i = 0; i < 100; i++) {
			String msg = "Message_" + i;
			channel.basicPublish("", QUEUE_NAME, null, msg.getBytes());
			System.out.println("第" + i + "消息发送成功");
		}
	}
}
