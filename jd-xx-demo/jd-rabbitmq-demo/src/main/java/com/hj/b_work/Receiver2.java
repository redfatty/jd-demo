package com.hj.b_work;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Receiver2 {
	private final static String QUEUE_NAME = "work";

	public static void main(String[] args) throws IOException, TimeoutException {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("192.168.232.131");
		factory.setPort(5672);
		factory.setVirtualHost("/hj");
		factory.setUsername("hj");
		factory.setPassword("hj");
		Connection connection = factory.newConnection();

		Channel channel = connection.createChannel();

		//每次预取一个,处理完再取下一个, 避免消息堆积
		channel.basicQos(1);

		channel.queueDeclare(QUEUE_NAME, false, false, false, null);

		//将autoAck设为false, 消费者调用相关方法来主动反馈
		channel.basicConsume(QUEUE_NAME, false, (consumerTag, message) -> {
			//收到消息回调
			String content = new String(message.getBody());
			System.out.println("Receiver1收到消息: " + content);
			//主动反馈已处理消息,可以接收下一个消息了
			channel.basicAck(message.getEnvelope().getDeliveryTag(), false);
		}, (consumerTag) -> {
			//取消回调
		});
	}
}
