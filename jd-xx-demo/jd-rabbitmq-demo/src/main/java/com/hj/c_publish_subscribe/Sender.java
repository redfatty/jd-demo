package com.hj.c_publish_subscribe;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Sender {
	//定义一个交换机的名称
	private final static String EXCHANGE_NAME = "fanout_exchange";

	public static void main(String[] args) throws IOException, TimeoutException {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("192.168.232.131");
		factory.setPort(5672);
		factory.setVirtualHost("/hj");
		factory.setUsername("hj");
		factory.setPassword("hj");
		Connection connection = factory.newConnection();

		Channel channel = connection.createChannel();
		//声明一个交换机
		channel.exchangeDeclare(EXCHANGE_NAME, "fanout");

		//注意如果消息发布后, 没有队列绑定到交换机, 则消息会清除
		//所以先启动两个消费者

		for (int i = 0; i < 100; i++) {
			String msg = "message_" + i;
			channel.basicPublish(EXCHANGE_NAME, "", null, msg.getBytes());
			System.out.println("发送消息_" + i + " 到交换机");
		}
	}
}
