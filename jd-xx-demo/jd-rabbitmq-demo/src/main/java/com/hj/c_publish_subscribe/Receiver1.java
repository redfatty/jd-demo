package com.hj.c_publish_subscribe;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Receiver1 {
	private final static String QUEUE_NAME = "publish_subscribe_1";
	private final static String EXCHANGE_NANE = "fanout_exchange";

	public static void main(String[] args) throws IOException, TimeoutException {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("192.168.232.131");
		factory.setPort(5672);
		factory.setVirtualHost("/hj");
		factory.setUsername("hj");
		factory.setPassword("hj");
		Connection connection = factory.newConnection();

		Channel channel = connection.createChannel();
		channel.basicQos(1);

		//声明队列
		channel.queueDeclare(QUEUE_NAME, false, false, false, null);
		//将队列跟交换机绑定
		channel.queueBind(QUEUE_NAME, EXCHANGE_NANE, "");

		//消费定义
		channel.basicConsume(QUEUE_NAME, false, (consumerTag, message) -> {
			//收到消息回调
			try {
				Thread.sleep(100L);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			String content = new String(message.getBody());
			System.out.println("Receiver1收到消息: " + content);

			//反馈
			channel.basicAck(message.getEnvelope().getDeliveryTag(), false);
		}, (consumerTag) -> {
			//取消回调
		});
	}
}
