package com.hj.d_routing;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Receiver1 {
	private final static String EXCHANGE_NAME = "direct_exchange";
	private final static String QUEUE_NAME = "direct_queue1";

	public static void main(String[] args) throws IOException, TimeoutException {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("192.168.232.131");
		factory.setPort(5672);
		factory.setVirtualHost("/hj");
		factory.setUsername("hj");
		factory.setPassword("hj");
		Connection connection = factory.newConnection();

		Channel channel = connection.createChannel();
		channel.basicQos(1);

		//声明队列
		channel.queueDeclare(QUEUE_NAME, false, false, false, null);

		//将队列绑定到交换机及路由key
		channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, "NBA");
		channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, "CBA");

		//消费定义
		channel.basicConsume(QUEUE_NAME, false, (consumerTag, message) -> {
			String content = new String(message.getBody());
			System.out.println("Receiver1收到消息: " + content);

			//反馈
			channel.basicAck(message.getEnvelope().getDeliveryTag(), false);
		}, (consumerTag) -> {

		});

	}
}
