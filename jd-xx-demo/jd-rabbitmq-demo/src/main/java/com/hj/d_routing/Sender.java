package com.hj.d_routing;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Sender {
	private final static String EXCHANGE_NAME = "direct_exchange";

	public static void main(String[] args) throws IOException, TimeoutException {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("192.168.232.131");
		factory.setPort(5672);
		factory.setVirtualHost("/hj");
		factory.setUsername("hj");
		factory.setPassword("hj");
		Connection connection = factory.newConnection();

		Channel channel = connection.createChannel();

		//声明交换机
		channel.exchangeDeclare(EXCHANGE_NAME, "direct");

		//发送消息到交换机, 并指定路由key
		channel.basicPublish(EXCHANGE_NAME, "NBA", null, "看篮球还得是NBA".getBytes());
		;
		channel.basicPublish(EXCHANGE_NAME, "CBA", null, "其实CBA也还不错啦".getBytes());
		System.out.println("消息发送成功");
	}
}
