package com.hj.e_topic;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Sender {
	private final static String EXCHANGE_NAME = "topic_exchange";

	public static void main(String[] args) throws IOException, TimeoutException {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("192.168.232.131");
		factory.setPort(5672);
		factory.setVirtualHost("/hj");
		factory.setUsername("hj");
		factory.setPassword("hj");
		Connection connection = factory.newConnection();

		Channel channel = connection.createChannel();
		channel.basicQos(1);

		//声明一个交换机
		channel.exchangeDeclare(EXCHANGE_NAME, "topic");

		//发消息
		channel.basicPublish(EXCHANGE_NAME, "product.add", null, "添加了一个商品".getBytes());
		channel.basicPublish(EXCHANGE_NAME, "product.delete", null, "删除了一个商品".getBytes());
		channel.basicPublish(EXCHANGE_NAME, "product.delete.other", null, "删除了其它商品".getBytes());

		System.out.println("消息发送成功");

	}
}
