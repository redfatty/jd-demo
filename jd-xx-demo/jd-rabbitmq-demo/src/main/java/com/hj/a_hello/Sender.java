package com.hj.a_hello;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import javax.lang.model.element.QualifiedNameable;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Sender {
	private final static String QUEUE_NAME = "hello";

	public static void main(String[] args) throws IOException, TimeoutException {

		//工厂创建构建连接
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("192.168.232.131");
		factory.setPort(5672);
		factory.setVirtualHost("/hj");
		factory.setUsername("hj");
		factory.setPassword("hj");
		Connection connection = factory.newConnection();

		//消息发送通道
		Channel channel = connection.createChannel();

		//声明一个队列
		channel.queueDeclare(QUEUE_NAME, false, false, false, null);

		//发送消息
		String msg = "Hello 2019!";
		channel.basicPublish("", QUEUE_NAME, null, msg.getBytes());
		System.out.println("消息发送成功");
	}
}
