package com.hj.a_hello;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Receiver {
	private final static String QUEUE_NAME = "hello";

	public static void main(String[] args) throws IOException, TimeoutException {
		//工厂构建连接对象
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("192.168.232.131");
		factory.setPort(5672);
		factory.setVirtualHost("/hj");
		factory.setUsername("hj");
		factory.setPassword("hj");
		Connection connection = factory.newConnection();

		//通道channel
		Channel channel = connection.createChannel();
		//声明队列,如果已经存在,就不会重新创建队列
		channel.queueDeclare(QUEUE_NAME, false, false, false, null);

		//将autoAck设为true,就是自动回复已收到消息,而不用消费者主动告知
		channel.basicConsume(QUEUE_NAME, true, (consumerTag, message) -> {
			//收到消息回调
			//String consumerTag, Delivery message
			String content = new String(message.getBody());
			System.out.println("Receiver收到消息: " + content);
		}, (consumerTag) -> {
			//取消回调
		});
	}
}
