package com.hj.jd.api.itemview;
import com.hj.jd.basic.entity.Product;
import com.hj.jd.basic.pojo.ResultBean;

import java.util.List;

public interface IItemViewService {
	ResultBean createItemView(Product product);

	ResultBean batchCreateItemView(List<Product> products);
}
