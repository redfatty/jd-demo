package com.hj.jd.api.order;

/**
 * 订单最新状态
 */
public interface OrderLatestState {
	int CREATED = 0;	//已创建,待支付
	int PAID = 1;		//已支付,待发货
	int SENT = 2;		//已发货,待收货
	int RECEIVED = 3;	//已收货,待评论
	int COMMENTED = 4;	//已评论, 待对账
	int FINISHED = 5;	//完成
}
