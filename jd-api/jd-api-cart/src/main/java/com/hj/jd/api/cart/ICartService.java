package com.hj.jd.api.cart;

import com.hj.jd.basic.pojo.ResultBean;

public interface ICartService {
	/**
	 * 添加购物车条目
	 * @param cartKey
	 * @param productId
	 * @param productCount
	 * @return 如果添加成功, data表示当前购物中的条目数量
	 */
	ResultBean addCartItem(String cartKey, Long productId, Integer productCount);

	/**
	 * 获取购物车
	 * @param cartKey
	 * @return
	 */
	ResultBean getCart(String cartKey);

	/**
	 * 获取前端视图购物车
	 * @param cartKey
	 * @return
	 */
	ResultBean getCartVO(String cartKey);

	/**
	 * 重置某个购物车条目的数量
	 * @param cartKey
	 * @param productId
	 * @param productCount
	 * @return
	 */
	ResultBean resetCartItemCount(String cartKey, Long productId, Integer productCount);

	/**
	 * 移除购物车条目
	 * @param cartKey
	 * @param productId
	 * @return
	 */
	ResultBean removeCartItem(String cartKey, Long productId);

	/**
	 * 合并购物车
	 * @param anonymousCartKey
	 * @param userCartKey
	 * @return
	 */
	ResultBean mergeCarts(String anonymousCartKey, String userCartKey);
}
