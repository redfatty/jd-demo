package com.hj.jd.api.cart;

import com.hj.jd.basic.entity.Product;

import java.io.Serializable;
import java.util.Date;

public class CartItemVO implements Serializable {
	private Product product;
	private Integer productCount;
	private Date updateTime;

	public CartItemVO() {
	}

	public CartItemVO(Product product, Integer productCount, Date updateTime) {
		this.product = product;
		this.productCount = productCount;
		this.updateTime = updateTime;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Integer getProductCount() {
		return productCount;
	}

	public void setProductCount(Integer productCount) {
		this.productCount = productCount;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@Override
	public String toString() {
		return "CartItemVO{" +
				"product=" + product +
				", productCount=" + productCount +
				", updateTime=" + updateTime +
				'}';
	}
}
