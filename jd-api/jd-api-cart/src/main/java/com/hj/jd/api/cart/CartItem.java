package com.hj.jd.api.cart;

import java.io.Serializable;
import java.util.Date;

public class CartItem implements Serializable {
	private Long productId;
	private Integer productCount;
	private Date updateTime;

	public CartItem() {
	}

	public CartItem(Long productId, Integer productCount, Date updateTime) {
		this.productId = productId;
		this.productCount = productCount;
		this.updateTime = updateTime;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Integer getProductCount() {
		return productCount;
	}

	public void setProductCount(Integer productCount) {
		this.productCount = productCount;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@Override
	public String toString() {
		return "CartItem{" +
				"productId=" + productId +
				", productCount=" + productCount +
				", updateTime=" + updateTime +
				'}';
	}
}
