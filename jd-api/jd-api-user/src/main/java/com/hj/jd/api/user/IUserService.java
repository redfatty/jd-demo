package com.hj.jd.api.user;

import com.hj.jd.basic.entity.User;
import com.hj.jd.basic.pojo.ResultBean;

public interface IUserService {
	/**
	 * 手机号是否已经注册
	 * @param phone
	 * @return
	 */
	boolean isRegisteredPhone(String phone);

	/**
	 * 登录
	 * @param loginname, 用户名,或手机号,或邮箱
	 * @param password
	 * @return
	 */
	User login(String loginname, String password);

	/**
	 * 登出
	 * @param token
	 */
	void logout(String token);

	/**
	 * 登录检测
	 * @param token
	 * @return
	 */
	ResultBean checkLogin(String token);

	Long add(User user);
}
