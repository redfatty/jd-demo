package com.hj.jd.api.product;


import com.hj.jd.basic.base.IBaseService;
import com.hj.jd.basic.entity.ProductType;

public interface IProductTypeService extends IBaseService<ProductType> {


}
