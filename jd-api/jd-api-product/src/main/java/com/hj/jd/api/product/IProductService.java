package com.hj.jd.api.product;


import com.hj.jd.basic.base.IBaseService;
import com.hj.jd.basic.entity.Product;

import java.util.List;

public interface IProductService extends IBaseService<Product> {

	/**
	 * 添加商品及描述
	 * @param product
	 * @return 表记录id
	 */
	Long saveProductAndDesc(Product product);
}
