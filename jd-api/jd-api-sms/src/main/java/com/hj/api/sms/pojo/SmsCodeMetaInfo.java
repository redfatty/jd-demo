package com.hj.api.sms.pojo;

import java.io.Serializable;
import java.nio.channels.Pipe;

public class SmsCodeMetaInfo implements Serializable {
	private int type;
	private String code;
	private String phone;

	public SmsCodeMetaInfo() {
	}

	public SmsCodeMetaInfo(int type, String code, String phone) {
		this.type = type;
		this.code = code;
		this.phone = phone;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "SmsCodeMetaInfo{" +
				"phone='" + phone + '\'' +
				", code='" + code + '\'' +
				", type=" + type +
				'}';
	}
}
