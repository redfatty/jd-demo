package com.hj.api.sms.pojo;

public interface SmsCodeType {
	Integer REGISTER = 1;
	Integer REST_PASSWORD = 2;
	Integer FORGOT_PASSWORD = 3;
}
