package com.hj.api.sms;

import com.hj.api.sms.pojo.SmsResponse;

public interface ISmsService {

	SmsResponse sendSmsCode(String toPhone, String authCode);
}
