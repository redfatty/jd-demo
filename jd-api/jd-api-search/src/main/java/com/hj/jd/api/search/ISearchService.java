package com.hj.jd.api.search;


import com.hj.jd.basic.entity.Product;
import com.hj.jd.basic.pojo.PageBean;
import com.hj.jd.basic.pojo.ResultBean;

import java.util.List;

public interface ISearchService {
	/**
	 * 保存或更新商品索引
	 * @param product
	 * @return
	 */
	ResultBean saveOrUpdateProductIndex(Product product);

	/**
	 * 删除索引
	 * @param id
	 * @return
	 */
	ResultBean deleteProductIndexById(Long id);

	/**
	 * 删除索引
	 * @param query
	 * @return
	 */
	ResultBean deleteProductIndexByQuery(String query);


	/**
	 * 搜索商品
	 * @param keywords 关键字
	 * @param pageIndex 页码,从1开始
	 * @param pageSize 页大小
	 * @return
	 */
	PageBean<Product> searchProductByKewords(String keywords, int pageIndex, int pageSize);
}
