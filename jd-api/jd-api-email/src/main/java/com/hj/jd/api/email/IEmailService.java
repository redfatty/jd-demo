package com.hj.jd.api.email;

import com.hj.jd.basic.pojo.ResultBean;

public interface IEmailService {

	ResultBean sendEmail(String fromUser, String toUser, String subject, String text);

//	Integer saveFailedEmail(e email);
}
