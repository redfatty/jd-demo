package com.hj.jd.web.cart.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.google.gson.Gson;
import com.hj.jd.api.cart.CartItem;
import com.hj.jd.api.cart.ICartService;
import com.hj.jd.basic.constant.CookieConst;
import com.hj.jd.basic.constant.RedisConst;
import com.hj.jd.basic.entity.User;
import com.hj.jd.basic.pojo.ResultBean;
import com.hj.jd.basic.util.RedisUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.*;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadPoolExecutor;

@Controller
public class CartController {

	@Reference
	private ICartService cartService;

	@RequestMapping("/addCartItem/{productId}/{productCount}")
	@ResponseBody
	public ResultBean addCartItem(HttpServletRequest request,
								  HttpServletResponse response,
								  @CookieValue(name = CookieConst.USER_CART_UUID, required = false) String cartUuid,
								  @PathVariable("productId") Long productId,
								  @PathVariable("productCount") Integer productCount) {

		/**
		 * 注意:
		 * 目前的处理方案是不管登录与否, 都只往redis缓存中存购物数据,不会持久化到数据库中
		 */

		User user = (User) request.getAttribute("user");
		//已经登录
		if (user != null) {
			String userCartKey = RedisUtils.getUserCartKey(user.getId());
			//这里要更新吗??
			resetCartCookie(response, cartUuid);
			return cartService.addCartItem(userCartKey, productId, productCount);
		}

		//未登录
		//1.初始化购物车uuid
		if (cartUuid == null || cartUuid.length() <= 0) {
			cartUuid = UUID.randomUUID().toString();
		}

		//2.添加
		ResultBean resultBean = cartService.addCartItem(RedisUtils.getAnonymousCartKey(cartUuid), productId, productCount);

		//3.cookie
		resetCartCookie(response, cartUuid);

		return resultBean;
	}

	@RequestMapping("getCart")
	@ResponseBody
	public ResultBean getCart(HttpServletRequest request,
							  HttpServletResponse response,
							  @CookieValue(name = CookieConst.USER_CART_UUID, required = false) String cartUuid) {
		// 注意:返回给前端的应该是List<CartItemVO>, 而不是List<CartItem>
		User user = (User) request.getAttribute("user");
		//已经登录
		if (user != null) {
			//以用户id为唯一标识去redis缓存中取购物车
			ResultBean resultBean = cartService.getCartVO(RedisUtils.getUserCartKey(user.getId()));
			if (resultBean.getErrno() != ResultBean.RESULT_SUCCESS || resultBean.getData() == null) {
				//还没有购物车
				return ResultBean.errorBean("当前用户还没有购物车~");
			}
			//已经有购物车
			resetCartCookie(response, cartUuid);
			return resultBean;
		}

		if (cartUuid == null || cartUuid.length() <= 0) {
			//没有本地购物车,或已删除
			return ResultBean.errorBean("没有购物车");
		}

		ResultBean resultBean = cartService.getCartVO(RedisUtils.getAnonymousCartKey(cartUuid));
		if (resultBean.getErrno() != ResultBean.RESULT_SUCCESS) {
			return ResultBean.errorBean("没有购物车");
		}

		//重置购物车cookie
		resetCartCookie(response, cartUuid);

		return resultBean;
	}

	@RequestMapping("/showCartPage")
	public String showCartPage(HttpServletRequest request,
							   HttpServletResponse response,
							   Model model,
							   @CookieValue(name = CookieConst.USER_CART_UUID, required = false) String cartUuid) {
		ResultBean resultBean = this.getCart(request, response, cartUuid);
		model.addAttribute("cartVO", resultBean.getData());
		return "shopcart";
	}

	@RequestMapping("/resetCartItemCount/{productId}/{productCount}")
	@ResponseBody
	public ResultBean resetCartItemCount(HttpServletRequest request,
										 HttpServletResponse response,
										 @CookieValue(name = CookieConst.USER_CART_UUID, required = false) String cartUuid,
										 @PathVariable("productId") Long productId,
										 @PathVariable("productCount") Integer productCount) {
		User user = (User) request.getAttribute("user");
		//1.已经登录
		if (user != null) {
			ResultBean resultBean = cartService.resetCartItemCount(RedisUtils.getUserCartKey(user.getId()), productId, productCount);
			//这里要不要刷新?
			resetCartCookie(response, cartUuid);
			return resultBean;
		}

		//2.没有登录,没有购物车
		if (cartUuid == null || cartUuid.length() <= 0) {
			return ResultBean.errorBean("还没有购物车");
		}

		//3.没有登录,有购物车
		ResultBean resultBean = cartService.resetCartItemCount(RedisUtils.getAnonymousCartKey(cartUuid), productId, productCount);
		//重置购物车cookie
		resetCartCookie(response, cartUuid);

		return resultBean;
	}

	@RequestMapping("/removeCartItem/{productId}")
	@ResponseBody
	public ResultBean removeCartItem(HttpServletRequest request,
									 HttpServletResponse response,
									 @CookieValue(name = CookieConst.USER_CART_UUID, required = false) String cartUuid,
									 @PathVariable("productId") Long productId) {
		User user = (User) request.getAttribute("user");
		if (user != null) {
			ResultBean resultBean = cartService.removeCartItem(RedisUtils.getUserCartKey(user.getId()), productId);
			resetCartCookie(response, cartUuid);
			return resultBean;
		}

		if (cartUuid == null || cartUuid.length() <= 0) {
			return ResultBean.errorBean("还没有购物车");
		}

		ResultBean resultBean = cartService.removeCartItem(RedisUtils.getAnonymousCartKey(cartUuid), productId);
		//重置购物车cookie
		resetCartCookie(response, cartUuid);
		return resultBean;
	}

	@RequestMapping("/mergetCarts")
	@ResponseBody
	public ResultBean mergetCarts(HttpServletRequest request,
								 HttpServletResponse response,
								 @CookieValue(name = CookieConst.USER_CART_UUID, required = false) String cartUuid) {
		System.out.println("调用: CartController.mergetCarts");
		if (cartUuid == null || cartUuid.length() <= 0) {
			return ResultBean.errorBean("不存在Cookie缓存购物车,无需合并");
		}
;
		User user = (User) request.getAttribute("user");
		if (user == null) {
			return ResultBean.errorBean("用户未登录,无法合并");
		}

		//合并
		String anonymousCartKey = RedisUtils.getAnonymousCartKey(cartUuid);
		String userCartKey = RedisUtils.getUserCartKey(user.getId());
		ResultBean resultBean = cartService.mergeCarts(anonymousCartKey, userCartKey);

		//清空cookie
		resetCartCookie(response, cartUuid, 0);
		return resultBean;
	}


	private void resetCartCookie(HttpServletResponse response, String cartUuid) {
		resetCartCookie(response, cartUuid, (int) (RedisConst.CART_EXPIRE_DAYS*24*60*60));
	}

	private void resetCartCookie(HttpServletResponse response, String cartUuid, int maxAge) {
		Cookie cartCookie = new Cookie(CookieConst.USER_CART_UUID, cartUuid);
		cartCookie.setMaxAge(maxAge);
		cartCookie.setPath("/");
		cartCookie.setDomain(CookieConst.PARENT_DOMAIN);
		cartCookie.setHttpOnly(true);
		response.addCookie(cartCookie);
	}
}
