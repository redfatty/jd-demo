package com.hj.jd.web.cart.config;

import com.hj.jd.web.cart.interceptor.CheckAuthInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 因为拦截器是SpringMVC框架中的组件, 所以这个配置类要实现WebMvcConfigurer接口, 来提供mvc的一些配置
 */
@Configuration
public class CartWebConfig implements WebMvcConfigurer {

	@Autowired
	private CheckAuthInterceptor checkAuthInterceptor;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(checkAuthInterceptor);
	}
}
