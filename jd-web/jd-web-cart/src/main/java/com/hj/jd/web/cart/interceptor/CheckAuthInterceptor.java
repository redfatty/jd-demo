package com.hj.jd.web.cart.interceptor;

import com.alibaba.dubbo.config.annotation.Reference;
import com.hj.jd.api.user.IUserService;
import com.hj.jd.basic.constant.CookieConst;
import com.hj.jd.basic.pojo.ResultBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 用于检测登录状态的拦截器
 */
@Component
public class CheckAuthInterceptor implements HandlerInterceptor {

	@Reference
	private IUserService userService;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		Cookie[] cookies = request.getCookies();
		if (cookies == null || cookies.length <= 0) {
			return true;
		}

		//遍历cookie, 如果有用户登录凭证的cookie,则验证是否还有效,将当前用户信息保存起来
		String userToken = null;
		for (Cookie cookie : cookies) {
			if (CookieConst.USER_TOKEN.equals(cookie.getName())) {
				userToken = cookie.getValue();
				break;
			}
		}

		if (userToken != null && userToken.length() > 0) {
			ResultBean resultBean = userService.checkLogin(userToken);
			if (resultBean.getErrno() == ResultBean.RESULT_SUCCESS) {
				request.setAttribute("user", resultBean.getData());
			}
		}

		return true;
	}
}
