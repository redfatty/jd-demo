package com.hj.jd.web.itemview;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JdWebItemviewApplication {

	public static void main(String[] args) {
		SpringApplication.run(JdWebItemviewApplication.class, args);
	}

}

