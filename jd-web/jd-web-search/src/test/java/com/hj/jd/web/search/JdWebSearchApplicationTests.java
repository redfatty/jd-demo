package com.hj.jd.web.search;

import org.apache.http.client.utils.URLEncodedUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.omg.CORBA.PUBLIC_MEMBER;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

//@RunWith(SpringRunner.class)
//@SpringBootTest
public class JdWebSearchApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Test
	public void urlEncodeTest() {

		try {
			String encodedStr = URLEncoder.encode("你好", "UTF-8");
			System.out.println(encodedStr);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		try {
			System.out.println(URLDecoder.decode("%E4%BD%A0%E5%A5%BD", "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

}

