package com.hj.jd.web.search.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import com.hj.jd.api.search.ISearchService;
import com.hj.jd.basic.entity.Product;
import com.hj.jd.basic.pojo.PageBean;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

@Controller
@RequestMapping("/search")
public class SearchController {

	@Reference
	private ISearchService searchService;

	@RequestMapping("/page/{pageIndex}/{pageSize}")
	public String searchProductList(Model model,
									String keywords,
									@PathVariable("pageIndex") Integer pageIndex,
									@PathVariable("pageSize") Integer pageSize) {
		if (StringUtils.isAnyEmpty()) {
			keywords = "精选推荐";
			System.out.println(keywords);
		}
		PageBean<Product> page = searchService.searchProductByKewords(keywords, pageIndex, pageSize);
		page.setNavigatePages(3);
		model.addAttribute("page", page);
		return "searchList";
	}
}
