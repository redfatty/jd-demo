package com.jd.jd.web.homepage.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.hj.jd.api.product.IProductTypeService;
import com.hj.jd.basic.entity.ProductType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class HomepageController {

	@Reference
	private IProductTypeService productTypeService;

	@RequestMapping("/")
	public String homepage(Model model) {
		List<ProductType> productTypeList = productTypeService.list();
		model.addAttribute("productTypeList", productTypeList);
		return "index";
	}

	@RequestMapping("/productTypeList")
	@ResponseBody
	public List<ProductType> productTypeList() {
		return  productTypeService.list();
	}
}
