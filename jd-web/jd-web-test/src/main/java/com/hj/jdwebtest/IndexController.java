package com.hj.jdwebtest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

class User {
	String username;
	String password;
	int age;

	public User() {
	}

	public User(String username, String password, int age) {
		this.username = username;
		this.password = password;
		this.age = age;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
}

@Controller
public class IndexController {

	@RequestMapping("/index")
	@ResponseBody
	public List<User> index(int pageIndex, int pageSize) {
		List<User> list = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			list.add(new User("name_" + i, "pwd_" + i, i + 1));
		}
		return list;
	}

	@RequestMapping(name = "/index2", method = RequestMethod.POST)
	@ResponseBody
	public List<User> index2(int pageIndex, int pageSize) {
		List<User> list = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			list.add(new User("name_" + i, "pwd_" + i, i + 1));
		}
		return list;
	}
}
