package com.hj.jdwebtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JdWebTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(JdWebTestApplication.class, args);
	}

}

