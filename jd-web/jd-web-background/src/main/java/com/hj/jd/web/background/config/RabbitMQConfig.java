package com.hj.jd.web.background.config;

import com.hj.jd.basic.constant.MQConst;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {
	/**
	 * 商品消息发送到这台交换机
	 * @return
	 */
	@Bean
	public TopicExchange getProductExchange() {
		return new TopicExchange(MQConst.EXCHANGE_PRODUCT, true, false);
	}
}
