package com.hj.jd.web.background.controller;

import com.github.tobato.fastdfs.domain.MateData;
import com.github.tobato.fastdfs.domain.StorePath;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import com.hj.jd.basic.pojo.ResultBean;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.security.DigestInputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Controller
public class CommonUploadController {

	@Value("${images.server}")
	private String images_server;

	@Autowired
	private FastFileStorageClient storageClient;

	@RequestMapping("/commonUpload")
	@ResponseBody
	public ResultBean commonUpload(MultipartFile[] files) {
		if (files == null || files.length < 1) {
			return  ResultBean.errorBean("文件个数不能为空!");
		}

		int len = files.length;
		String[] urls = new String[len];

		for (int i = 0; i < len; i++) {
			MultipartFile file = files[i];
			String originalFilename = file.getOriginalFilename();
			String extraName = originalFilename.substring(originalFilename.lastIndexOf(".") + 1);
			try (InputStream inputStream = file.getInputStream()) {
				//上传文件, 并得到存储路径信息
				StorePath storePath = storageClient.uploadImageAndCrtThumbImage(inputStream, file.getSize(), extraName, null);
				//图片最终地址
				String fileUrl = images_server + storePath.getFullPath();
				System.out.println("图片地址:" + fileUrl);
				urls[i] = fileUrl;
			} catch (IOException e) {
				System.out.println("读取上传文件失败失败");
				return  ResultBean.errorBean("文件上传失败!");
			}
		}

		return ResultBean.successBean(urls);
	}

}
