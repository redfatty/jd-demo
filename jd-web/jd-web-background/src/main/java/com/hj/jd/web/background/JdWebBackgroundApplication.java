package com.hj.jd.web.background;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import com.github.tobato.fastdfs.FdfsClientConfig;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Import;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@Import(FdfsClientConfig.class)
@EnableDubbo
@EnableRabbit
public class JdWebBackgroundApplication {

	public static void main(String[] args) {
		SpringApplication.run(JdWebBackgroundApplication.class, args);
	}

}

