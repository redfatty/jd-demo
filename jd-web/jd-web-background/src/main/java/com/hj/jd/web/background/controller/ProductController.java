package com.hj.jd.web.background.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import com.github.tobato.fastdfs.domain.StorePath;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import com.google.gson.Gson;
import com.hj.jd.api.itemview.IItemViewService;
import com.hj.jd.api.product.IProductService;
import com.hj.jd.api.product.IProductTypeService;
import com.hj.jd.api.search.ISearchService;
import com.hj.jd.basic.constant.MQConst;
import com.hj.jd.basic.entity.Product;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItem;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/product")
public class ProductController {

    @Reference
    private IProductService productService;

    @Reference
    private IProductTypeService productTypeService;

    @Reference
    private ISearchService searchService;

    @Reference
    private IItemViewService itemViewService;

    @Autowired
    private FastFileStorageClient fastFileStorageClient;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @RequestMapping("/getById/{productId}")
    @ResponseBody
    public Product getById(@PathVariable("productId") Long productId) {
        Product product = productService.selectByPrimaryKey(productId);
        return productService.selectByPrimaryKey(productId);
    }

    @RequestMapping("/list")
    public String list(Model model) {
        List<Product> list = productService.list();
        model.addAttribute("productList", list);
        return  "productList";
    }

    @RequestMapping("/page/{pageIndex}/{pageSize}")
    public String page(Model model,
                       @PathVariable(value = "pageIndex") Integer pageIndex,
                       @PathVariable(value = "pageSize") Integer pageSize) {
        PageInfo<Product> pageInfo = productService.page(pageIndex, pageSize);
        model.addAttribute("page", pageInfo);
        return  "productList";
    }


    @RequestMapping("/toAdd")
    public String toAdd() {
        return "productAdd";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String add(Product product) {
        System.out.println("开始添加商品");
        product.setCreateUser(1L);
        product.setUpdateUser(1L);
        product.setFlag(true);
        Long id = productService.saveProductAndDesc(product);
        if (id != null && id > 0) {
            product.setId(id);

            rabbitTemplate.convertAndSend(MQConst.EXCHANGE_PRODUCT, MQConst.ROUTINGKEY_PRODUCT_ADD, product);

//            //添加到索引库
//            searchService.saveOrUpdateProductIndex(product);
//            //创建静态视图
//            itemViewService.createItemView(product);
        }
        return  "redirect:page/1/5";
    }

    @RequestMapping("/toUpdate/{productId}")
    public String toUpdate(@PathVariable("productId") Long productId, Model model) {
        Product product = productService.selectByPrimaryKey(productId);
        model.addAttribute("product", product);
        return "productUpdate";
    }

    @RequestMapping("/update")
    public String update(Product product) {
        productService.updateByPrimaryKeySelective(product);
        //更新索引库
        searchService.saveOrUpdateProductIndex(product);
        //更新静态视图
        itemViewService.createItemView(product);
        return "redirect:page";
    }

    @RequestMapping("/del/{productId}")
    public String del(@PathVariable("productId") Long productId) {
        System.out.println("删除...");
        int rows = productService.deleteByPrimaryKey(productId);
        if (rows > 0) {
            //发送删除商品的消息
            rabbitTemplate.convertAndSend(MQConst.EXCHANGE_PRODUCT, MQConst.ROUTINGKEY_PRODUCT_DELETE, productId);
        }
        return "redirect:page";
    }

    @RequestMapping("/typeListForJsonp")
    @ResponseBody
    public String typeListForJsonp(String callback) {
//        List<ProductType> typeList = productTypeService.list();
//        String typeListJson = new Gson().toJson(typeList);
        String typeListJson = "结果";
        return callback + "(" + typeListJson + ");";
    }
}
