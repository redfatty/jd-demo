package com.hj.jd.web.order.controller;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.hj.jd.basic.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.ReadOnlyProperty;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class OrderController {

	private final static String PAY_RETURN_URL = "";
	private static final String PAY_NOTIFY_URL = "order.jdd.com:9095/onPayNotify";

	@Autowired
	private AlipayClient alipayClient;

	@RequestMapping("/toConfirm")
	public String toConfirm(Model model) {
		//1.获取登录用户
		//2.获取用户地址列表
		//3.获取支付方式
		//4.获取快递方式
		//5.获取购物车信息
		model.addAttribute("orderItems", null);
		return "orderConfirm";
	}

	@RequestMapping("/create")
	public String createOrder(HttpServletRequest request) {
		User user = (User) request.getAttribute("user");

		//查库存->跟购买数量比较
		//1.库存不够, 100毫秒再重试
		//重试3次后,还是没不够,就提示用户
		//2.库存足够,锁库存(30分钟),再继续...

		//创建订单
		Date date = new Date();
		String dateTimeString = new SimpleDateFormat("yyyyMMddHHmmss").format(date);
		String orderNo = new StringBuilder(dateTimeString).append(user.getId()).toString();
		//订单状态

		//创建完订单后,跳转至支付页面

		return "";
	}

	@RequestMapping("/toPay/{orderNo}")
	private void toPay(HttpServletResponse response, @PathVariable("orderNo") String orderNo) {
		String CHARSET = "utf-8";
		AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();//创建API对应的request
		//设置回调地址
		alipayRequest.setReturnUrl(PAY_RETURN_URL);
		alipayRequest.setNotifyUrl(PAY_NOTIFY_URL);//在公共参数中设置回跳和通知地址
		alipayRequest.setBizContent("{" +
				"    \"out_trade_no\":\""+orderNo+"\"," +
				"    \"product_code\":\"FAST_INSTANT_TRADE_PAY\"," +
				"    \"total_amount\":88888.88," +
				"    \"subject\":\"小米笔记本\"," +
				"    \"body\":\"小米笔记本\"," +
				"    \"passback_params\":\"merchantBizType%3d3C%26merchantBizNo%3d2016010101111\"" +
				"  }");
		String form="";
		try {
			form = alipayClient.pageExecute(alipayRequest).getBody(); //调用SDK生成表单
		} catch (AlipayApiException e) {
			e.printStackTrace();
		}
		response.setContentType("text/html;charset=" + CHARSET);
		try {
			response.getWriter().write(form);//直接将完整的表单html输出到页面
			response.getWriter().flush();
			response.getWriter().close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@RequestMapping("/noPayNotify")
	public String noPayNotify() {
		return "payResult";
	}


}
