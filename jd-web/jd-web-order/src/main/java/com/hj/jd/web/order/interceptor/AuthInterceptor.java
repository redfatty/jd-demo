package com.hj.jd.web.order.interceptor;

import com.alibaba.dubbo.config.annotation.Reference;
import com.hj.jd.api.user.IUserService;
import com.hj.jd.basic.constant.CookieConst;
import com.hj.jd.basic.pojo.ResultBean;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class AuthInterceptor implements HandlerInterceptor {

	@Reference
	private IUserService userService;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		//登录校验
		Cookie[] cookies = request.getCookies();
		if (null != cookies && cookies.length > 0) {
			for (Cookie cookie : cookies) {
				if (CookieConst.USER_TOKEN.equals(cookie.getName())) {
					String userToken = cookie.getValue();
					ResultBean resultBean = userService.checkLogin(userToken);
					Object user = resultBean.getData();
					if (ResultBean.RESULT_SUCCESS == resultBean.getErrno() && null != user) {
						request.setAttribute("user", user);
						return true;
					}
				}
			}
		}

		//未登录
		//要把跳转之前的地址带过去,保证登录之后能正确地跳转回来
		String referer = request.getRequestURL().toString();
		response.sendRedirect("sso.jdd.com:9093/toLogin?referer=" + referer);
		return false;
	}
}
