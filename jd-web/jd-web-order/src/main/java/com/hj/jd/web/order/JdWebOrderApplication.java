package com.hj.jd.web.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class JdWebOrderApplication {

	public static void main(String[] args) {
		SpringApplication.run(JdWebOrderApplication.class, args);
	}

}

