package com.hj.jd.web.sso.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.google.gson.Gson;
import com.hj.api.sms.pojo.SmsCodeMetaInfo;
import com.hj.api.sms.pojo.SmsCodeType;
import com.hj.jd.api.user.IUserService;
import com.hj.jd.basic.constant.CookieConst;
import com.hj.jd.basic.constant.MQConst;
import com.hj.jd.basic.constant.RedisConst;
import com.hj.jd.basic.entity.User;
import com.hj.jd.basic.pojo.ResultBean;
import com.hj.jd.basic.util.CommonUtils;
import com.hj.jd.basic.util.RedisUtils;
import com.hj.jd.basic.utils.web.CookieUtils;
import com.hj.jd.basic.utils.web.HttpClientUtils;
import org.jboss.netty.util.internal.ReusableIterator;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sound.midi.Soundbank;
import java.awt.*;
import java.net.CookieManager;
import java.net.HttpCookie;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Controller
public class UserController {

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@Autowired
	private RedisTemplate redisTemplate;

	@Reference
	private IUserService userService;

	@RequestMapping("/")
	public String index(HttpSession session) {
		return "login";
	}

	/**
	 * 去注册页面
	 * @return
	 */
	@RequestMapping("/toRegister")
	public String toRegister() {
		return "register";
	}

	/**
	 * 邮箱注册
	 * @param model
	 * @param password
	 * @param email
	 * @return
	 */
	@RequestMapping("/registerByEmail")
	public String registerByEmail(Model model, String password, String email) {
		User user = new User();
		user.setPassword(password);
		user.setEmail(email);
		//Long userId = userService.add(user)
		rabbitTemplate.convertAndSend(MQConst.EXCHANGE_USER, MQConst.ROUTINGKEY_USER_REGISTER_EMAIL, user);
		model.addAttribute("loginname", email);
		return "forward:toLogin";
	}

	/**
	 * 手机注册
	 * @param session
	 * @param model
	 * @param password
	 * @param phone
	 * @param authCode
	 * @return
	 */
	@RequestMapping("/registerByPhone")
	public String registerByPhone(HttpSession session,
								  Model model,
								  String password,
								  String phone,
								  String authCode) {
		//1.手机号是不是刚才那个
		String phoneSaved = (String) session.getAttribute("phone");
		if (!phone.equals(phoneSaved)) {
			System.out.println("手机号码不对");
			return "forward:toRegister";
		}

		//2.验证码是否正确
		Object codeSaved = redisTemplate.opsForValue().get("register:code" + phone);
		if (!authCode.equals(codeSaved)) {
			System.out.println("验证码错误或者已过期");
			return "forward:toRegister";
		}

		//3.通过验证,删除验证码和手机号
		redisTemplate.delete("register:code" + phone);
		session.removeAttribute("phone");

		//4.注册
		User user = new User();
		user.setPassword(password);
		user.setPhone(phone);
		Long userId = userService.add(user);

		//5.跳转至登录页面
		model.addAttribute("loginname", phone);
		return "forward:toLogin";
	}

	/**
	 * 发送短信验证码
	 * @param session
	 * @param phone
	 * @param type
	 * @return
	 */
	@RequestMapping("/sendSmsAuthCode")
	@ResponseBody
	public ResultBean sendSmsAuthCode(HttpSession session, String phone, Integer type) {
		//格式校验
		if (!CommonUtils.validatePhone(phone)) {
			return ResultBean.errorBean("手机号码不正确");
		}

		//注册时检验
		if (type == SmsCodeType.REGISTER) {
			if (userService.isRegisteredPhone(phone)) {
				return ResultBean.errorBean("该手机号已经注册");
			}
		}

		//修改密码时检验
		if (type == SmsCodeType.REST_PASSWORD || type == SmsCodeType.FORGOT_PASSWORD) {
			if (!userService.isRegisteredPhone(phone)) {
				return ResultBean.errorBean("该手机号还没有注册");
			}
		}

		//手机号保存到session中
		session.setAttribute("phone", phone);

		//生成验证码
		String authCode = "123456";

		//将验证码保存至redis
		String codeKey = "register:code:" + phone;//应该判断类型
		redisTemplate.opsForValue().set(codeKey, authCode);
		redisTemplate.expire(codeKey, 3, TimeUnit.MINUTES);

		//发消息到交换机
		SmsCodeMetaInfo info = new SmsCodeMetaInfo(type, authCode, phone);
		rabbitTemplate.convertAndSend(MQConst.EXCHANGE_USER, MQConst.ROUTINGKEY_USER_SEND_SMS_CODE, info);

		return ResultBean.successBean(authCode);
	}

	/**
	 * 去登录页面
	 * @return
	 */
	@RequestMapping("/toLogin")
	public String toLogin(HttpServletRequest request, Model model) {
		//从来哪的
		//登录页面将这个地址保存起来,登录请求中携带过来
		//有可能是拦截器跳转到这来的,从请求参数中获取
		String referer = request.getParameter("referer");
		if (null == referer) {
			referer = request.getHeader("referer");
		}
		model.addAttribute("referer", referer);
		return "login";
	}

	/**
	 * 登录
	 * @param response
	 * @param loginname
	 * @param password
	 * @return
	 */
	@RequestMapping("/login")
	public String login(HttpServletResponse response,
						String loginname,
						String password,
						String referer,
						@CookieValue(name = CookieConst.USER_CART_UUID, required = false) String cartKey) {
		User user = userService.login(loginname, password);
		System.out.println(user);
		if (user == null) {
			//用户名或密码错误
			return "forward:toLogin";
		}
		//登录成功
		//1.将token写入浏览器cookie
		String userToken = user.getToken();
		CookieUtils.addUserTokenCookie(response, userToken);

		//2.合并购物车
		//"user_cart_uuid=xxxx;user_token=xxxx"
		StringBuilder cookieValue = new StringBuilder(CookieConst.USER_CART_UUID).append("=").append(cartKey).append(";");
		cookieValue.append(CookieConst.USER_TOKEN).append("=").append(userToken);
		Map<String, String>cookieHeaders = new HashMap<>();
		cookieHeaders.put("Cookie", cookieValue.toString());
		String mergeResult = HttpClientUtils.doGet("http://cart.jdd.com/mergetCarts", null, cookieHeaders);
		System.out.println("合并结果: " + mergeResult);

		//2.跳转
		if (referer == null || referer.trim().length() <= 0) {
			return "redirect:http://homepage.jdd.com/";
		} else {
			return "redirect:" + referer;
		}
	}

	/**
	 * 注销登出
	 * @return
	 */
	@RequestMapping("/logout")
	@ResponseBody
	public ResultBean logout(HttpServletResponse response, @CookieValue(name = CookieConst.USER_TOKEN, required =  false) String token) {
		return logoutHandler(response, token);
	}

	/**
	 * 注销登出 跨域调用
	 * @return
	 */
	@RequestMapping("/logoutForJsonp")
	@ResponseBody
	public String logoutForJsonp(HttpServletResponse response, @CookieValue(name = CookieConst.USER_TOKEN, required =  false) String token, String callback) {
		String jsonResult = new Gson().toJson(logoutHandler(response, token));
		return callback + "(" + jsonResult + ")";
	}

	/**
	 * 登录状态检测
	 * @param token
	 * @return
	 */
	@RequestMapping("/checkLogin")
	@ResponseBody
	public ResultBean checkLogin(@CookieValue(name = CookieConst.USER_TOKEN, required = false) String token) {
		return  checkLoginHandler(token);
	}

	/**
	 * 登录状态检测
	 * 跨域调用
	 * @param token
	 * @param callback
	 * @return
	 */
	@RequestMapping("/checkLoginForJsonp")
	@ResponseBody
	public String checkLoginForJsonp(@CookieValue(name = CookieConst.USER_TOKEN, required = false) String token, String callback) {
		String jsonResult = new Gson().toJson(checkLoginHandler(token));
		return callback + "(" + jsonResult + ")";
	}

	private ResultBean checkLoginHandler(String token) {
		ResultBean resultBean = null;
		if (token == null) {
			resultBean = ResultBean.errorBean("还未登录");
		} else {
			resultBean = userService.checkLogin(token);
		}
		return  resultBean;
	}

	private ResultBean logoutHandler(HttpServletResponse response, String token) {
		if (token == null) {
			return ResultBean.errorBean("当前用户未登录~");
		}

		//1.删除cookie
		CookieUtils.deleteUserTokenCookie(response, token);
		//2.删除redis中的用户信息
		userService.logout(token);
		return ResultBean.successBean("登出成功~");
	}

	@RequestMapping("/cookie")
	@ResponseBody
	public void cookie() {
		HttpCookie cookie = new HttpCookie("password", "123456789");
		CookieManager manager = new CookieManager();
		try {
			manager.getCookieStore().add(new URI("http://localhost:9094"), cookie);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}
}
