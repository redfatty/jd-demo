package com.hj.jd.web.sso.config;

import com.hj.jd.basic.constant.MQConst;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//@Configuration
public class RabbitmqConfig {

	@Bean
	public TopicExchange getTopicExchange() {
		return new TopicExchange(MQConst.EXCHANGE_USER, true, false);
	}
}
