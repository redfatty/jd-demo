package com.hj.jd.web.sso;

import com.alibaba.dubbo.config.annotation.Reference;
import com.hj.jd.api.user.IUserService;
import com.hj.jd.basic.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JdWebSsoApplicationTests {

	@Reference
	private IUserService userService;

	@Test
	public void contextLoads() {
		User user = userService.login("admin", "admin");
		System.out.println(user);
	}

}

