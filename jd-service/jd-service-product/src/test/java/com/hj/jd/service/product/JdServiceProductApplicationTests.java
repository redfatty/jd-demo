package com.hj.jd.service.product;

import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import com.hj.jd.api.product.IProductService;
import com.hj.jd.basic.entity.Product;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JdServiceProductApplicationTests {

	@Reference
	private IProductService productService;

	@Test
	public void contextLoads() {
		PageInfo<Product> pageInfo = productService.page(1, 2);
		List<Product> list = pageInfo.getList();
		for (Product product : list) {
			System.out.println(list);
		}
	}

}

