package com.hj.jd.service.product;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableDubbo
@MapperScan("com.jd.basic.mapper")//提供服务时,依赖mapper
public class JdServiceProductApplication {

	public static void main(String[] args) {
		SpringApplication.run(JdServiceProductApplication.class, args);
	}

}

