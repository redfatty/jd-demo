package com.hj.jd.service.product.impl;

import com.hj.jd.api.product.IProductService;
import com.hj.jd.basic.base.BaseServiceImpl;
import com.hj.jd.basic.base.IBaseDao;
import com.hj.jd.basic.entity.Product;
import com.hj.jd.basic.entity.ProductDesc;
import com.jd.basic.mapper.ProductDescMapper;
import com.jd.basic.mapper.ProductMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class ProductServiceImpl extends BaseServiceImpl<Product> implements IProductService {

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private ProductDescMapper productDescMapper;

    @Override
    public IBaseDao<Product> getBaseDao() {
        return productMapper;
    }

    @Override
    public Long saveProductAndDesc(Product product) {
        //持久化商品信息
        insertSelective(product);
        //持久化商品描述信息
        ProductDesc productDesc = product.getProductDesc();
        productDesc.setProductId(product.getId());
        productDescMapper.insertSelective(productDesc);

        return product.getId();
    }
}
