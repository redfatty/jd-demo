package com.hj.jd.service.product.impl;

import com.hj.jd.api.product.IProductTypeService;
import com.hj.jd.basic.base.BaseServiceImpl;
import com.hj.jd.basic.base.IBaseDao;
import com.hj.jd.basic.entity.ProductType;
import com.jd.basic.mapper.ProductTypeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class ProductTypeServiceImpl extends BaseServiceImpl<ProductType> implements IProductTypeService {

	@Autowired
	private ProductTypeMapper productTypeMapper;

	@Override
	public IBaseDao<ProductType> getBaseDao() {
		return productTypeMapper;
	}
}
