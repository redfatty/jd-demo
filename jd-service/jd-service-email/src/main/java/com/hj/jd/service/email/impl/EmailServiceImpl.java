package com.hj.jd.service.email.impl;

import com.hj.jd.api.email.IEmailService;
import com.hj.jd.basic.entity.EmailTodo;
import com.hj.jd.basic.pojo.ResultBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMailMessage;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.InputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class EmailServiceImpl implements IEmailService {
	private static final Integer AVAILABLE_THREADS = Runtime.getRuntime().availableProcessors();
	private static ExecutorService threadPool;
	static {
		threadPool = new ThreadPoolExecutor(
				AVAILABLE_THREADS,
				AVAILABLE_THREADS*2,
				10L,
				TimeUnit.MILLISECONDS,
				new LinkedBlockingQueue<Runnable>(100));
	}

	@Autowired
	private JavaMailSender mailSender;

	@Override
	public ResultBean sendEmail(String fromUser, String toUser, String subject, String text) {
		boolean isSuccess = false;
		MimeMessage message = mailSender.createMimeMessage();
		try {
			//封闭邮件数据
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setFrom(fromUser);
			helper.setTo(toUser);
			helper.setSubject(subject);
			helper.setText(text, true);
			//发送
			mailSender.send(message);
			isSuccess = true;
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (isSuccess) {
			return ResultBean.successBean("邮件发送成功");
		} else {
			EmailTodo emailTodo = new EmailTodo();
			emailTodo.setToUser(toUser);
			emailTodo.setSubject(subject);
			emailTodo.setContent(text);
			emailTodo.setCount(1);
			//将发送失败的邮件保存起来
			return ResultBean.errorBean("邮件发送失败");
		}
	}
}
