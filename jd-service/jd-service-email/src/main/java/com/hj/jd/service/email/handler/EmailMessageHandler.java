package com.hj.jd.service.email.handler;

import com.hj.jd.api.email.IEmailService;
import com.hj.jd.basic.constant.MQConst;
import com.hj.jd.basic.entity.EmailTodo;
import com.hj.jd.basic.entity.User;
import com.hj.jd.basic.pojo.ResultBean;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.concurrent.*;

@Component
public class EmailMessageHandler {

	@Autowired
	private IEmailService emailService;

	@Value("${spring.mail.username}")
	private String email_from;

	/**
	 * 用户注册
	 * @param user
	 */
	@RabbitListener(queues = MQConst.QUEUE_USER_REGISTER_EMAIL)
	public void processUserRegisterMessage(User user) {
		String subject = "账户激活";
		String text = "点击链接,完成激活<a href='http://www.baidu.com'> http://www.baidu.com </a>";
		ResultBean resultBean = emailService.sendEmail(email_from, user.getEmail(), subject, text);
		boolean isSuccess = resultBean.getErrno() == ResultBean.RESULT_SUCCESS;
	}
}
