package com.hj.jd.service.email.config;

import com.hj.jd.basic.constant.MQConst;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitmqConfig {

	@Bean
	public TopicExchange getTopicExchange() {
		return new TopicExchange(MQConst.EXCHANGE_USER, true, false);
	}

	@Bean
	public Queue getUserRegisterWithEmailMsgQueue() {
		return new Queue(MQConst.QUEUE_USER_REGISTER_EMAIL, true, false, false);
	}

	@Bean
	public Binding getUserRegisterWithEmailMsgBinding(TopicExchange getTopicExchange,
													  Queue getUserRegisterWithEmailMsgQueue) {
		return BindingBuilder.bind(getUserRegisterWithEmailMsgQueue)
				.to(getTopicExchange)
				.with(MQConst.ROUTINGKEY_USER_REGISTER_EMAIL);
	}
}
