package com.hj.jd.service.email;

import com.hj.jd.api.email.IEmailService;
import com.hj.jd.basic.constant.MQConst;
import com.hj.jd.basic.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JdServiceEmailApplicationTests {

	@Autowired
	private IEmailService emailService;

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@Test
	public void contextLoads() {
	}

	@Test
	public void sendEmailTest() {

	}

	@Test
	public void emailMessageTest() {
	}

}

