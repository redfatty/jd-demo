package com.hj.jd.service.search.handler;

import com.hj.jd.api.search.ISearchService;
import com.hj.jd.basic.constant.MQConst;
import com.hj.jd.basic.entity.Product;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProductMessageHandler {

	@Autowired
	private ISearchService searchService;

	@RabbitListener(queues = MQConst.QUEUE_PRODUCT_ADD)
	public void processProductAddingMessage(Product product) {
		searchService.saveOrUpdateProductIndex(product);
	}

	@RabbitListener(queues = MQConst.QUEUE_PRODUCT_DELETE)
	public void processProductDeletionMessage(Long productId) {
		searchService.deleteProductIndexById(productId);
	}
}
