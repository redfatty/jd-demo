package com.hj.jd.service.search.impl;

import com.hj.jd.api.search.ISearchService;
import com.hj.jd.basic.entity.Product;
import com.hj.jd.basic.pojo.PageBean;
import com.hj.jd.basic.pojo.ResultBean;
import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class SearchServiceImpl implements ISearchService {

	@Autowired
	private SolrClient solrClient;

	@Override
	public ResultBean saveOrUpdateProductIndex(Product product) {
		SolrInputDocument document = new SolrInputDocument();
		document.setField("id", product.getId());
		document.setField("product_name", product.getName());
		document.setField("product_price", product.getPrice());
		document.setField("product_sale_point", product.getSalePoint());
		document.setField("product_image", product.getImage());

		try {
			solrClient.add(document);
			solrClient.commit();
		} catch (SolrServerException | IOException addEx) {
			addEx.printStackTrace();
			try {
				solrClient.rollback();
			} catch (SolrServerException | IOException rbEx) {
				rbEx.printStackTrace();
			} finally {
				return ResultBean.errorBean("添加商品索引失败");
			}
		}

		return ResultBean.successBean("添加商品索引成功");
	}

	@Override
	public ResultBean deleteProductIndexById(Long id) {
		try {
			solrClient.deleteById(id.toString());
		} catch (SolrServerException | IOException e) {
			e.printStackTrace();
			return  ResultBean.errorBean("删除商品索引失败");
		}

		return ResultBean.successBean("删除商品索引成功");
	}

	@Override
	public ResultBean deleteProductIndexByQuery(String query) {
		try {
			solrClient.deleteByQuery(query);
		} catch (SolrServerException | IOException e) {
			e.printStackTrace();
			return  ResultBean.errorBean("删除商品索引失败");
		}
		return ResultBean.successBean("删除商品索引成功");
	}

	@Override
	public PageBean<Product> searchProductByKewords(String keywords, int pageIndex, int pageSize) {
		if (StringUtils.isAnyEmpty(keywords) || pageIndex == 0 || pageSize == 0) {
			return  PageBean.nonePage();
		}

		SolrQuery query = new SolrQuery();
		//关键字,分页,高亮
		query.setQuery("product_keywords:" + keywords);
		query.setStart((pageIndex - 1) * pageSize);
		query.setRows(pageSize);

		query.setHighlight(true);
		query.addHighlightField("product_name");
		query.addHighlightField("product_sale_point");
		query.setHighlightSimplePre("<font color='red'>");
		query.setHighlightSimplePost("</font>");

		try {
			QueryResponse response = solrClient.query(query);
			SolrDocumentList documents = response.getResults();
			Map<String, Map<String, List<String>>> hls = response.getHighlighting();
			ArrayList<Product> list = new ArrayList<>(documents.size());
			PageBean pageBean = new PageBean();

			for (SolrDocument document : documents) {
				Product product = new Product();
				list.add(product);
				//不高亮的信息
				product.setId(Long.parseLong(document.getFieldValue("id").toString()));
				product.setPrice(Long.parseLong(document.getFieldValue("product_price").toString()));
				product.setImage(document.getFieldValue("product_image").toString());

				//商品名高亮信息
				Map<String, List<String>> productHls = hls.get(document.getFieldValue("id"));
				List<String> nameHls = productHls.get("product_name");
				if(nameHls != null && nameHls.size() > 0){
					product.setName(nameHls.get(0));
				}else{
					product.setName(document.getFieldValue("product_name").toString());
				}
				//商品卖点高亮信息
				List<String> salePointHls = productHls.get("product_sale_point");
				if(salePointHls != null && salePointHls.size() > 0){
					product.setSalePoint(salePointHls.get(0));
				}else{
					product.setSalePoint(document.getFieldValue("product_sale_point").toString());
				}
			}

			long total = documents.getNumFound();
			pageBean.setPageNum(pageIndex);
			pageBean.setPageSize(pageSize);
			pageBean.setList(list);
			pageBean.setTotal(total);
			int pages = (int) (total / pageSize);
			if (total % pageSize != 0) {
				pages++;
			}
			pageBean.setPages(pages);

			return pageBean;
		} catch (SolrServerException | IOException e) {
			e.printStackTrace();
			//记录异常日志信息
			return PageBean.nonePage();
		}
	}
}
