package com.hj.jd.service.search.config;

import com.hj.jd.basic.constant.MQConst;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

	@Bean
	public TopicExchange getTopicExchange() {
		return new TopicExchange(MQConst.EXCHANGE_PRODUCT, true, false);
	}

	@Bean
	public Queue getProductAddingQueue() {
		return new Queue(MQConst.QUEUE_PRODUCT_ADD, true, false, false);
	}

	@Bean
	public Queue getProductDeletionQueue() {
		return new Queue(MQConst.QUEUE_PRODUCT_DELETE, true, false, false);
	}

	@Bean
	public Binding getProductAddingBinding(TopicExchange getTopicExchange, Queue getProductAddingQueue) {
		return BindingBuilder.bind(getProductAddingQueue)
				.to(getTopicExchange)
				.with(MQConst.ROUTINGKEY_PRODUCT_ADD);
	}

	@Bean
	public Binding getProductDeletionBinding(TopicExchange getTopicExchange, Queue getProductDeletionQueue) {
		return BindingBuilder.bind(getProductDeletionQueue)
				.to(getTopicExchange)
				.with(MQConst.ROUTINGKEY_PRODUCT_DELETE);
	}
}
