package com.hj.jd.service.search;

import com.alibaba.dubbo.config.annotation.Reference;
import com.hj.jd.api.search.ISearchService;
import com.hj.jd.basic.entity.Product;
import com.hj.jd.basic.pojo.PageBean;
import com.hj.jd.basic.pojo.ResultBean;
import com.hj.jd.service.search.impl.SearchServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JdServiceSearchApplicationTests {

	@Reference
	private ISearchService searchService;

	@Test
	public void contextLoads() {
		Product product = new Product();
		product.setId(1L);
		product.setName("华为Mate20");
		product.setPrice(999L);
		product.setSalePoint("Almost东半球最好的用机");
		product.setImage("123.jpg");

		ResultBean resultBean = searchService.saveOrUpdateProductIndex(product);
		System.out.println(resultBean.getData());
		System.out.println(resultBean.getMessage());
	}

	@Test
	public void deleteProductIndexByIdTest() {
		ResultBean resultBean = searchService.deleteProductIndexById(2L);
		System.out.println(resultBean.getMessage());
		System.out.println(resultBean.getData());
	}

	@Test
	public void deleteProductIndexByQueryTest() {
		ResultBean resultBean = searchService.deleteProductIndexByQuery("product_name:华为");
		System.out.println(resultBean.getMessage());
		System.out.println(resultBean.getData());
	}

	@Test
	public void searchProductByKeywordsTest() {
		PageBean<Product> pageBean = searchService.searchProductByKewords("华为", 1, 5);
		for (Product product : pageBean.getList()) {
			System.out.println(product.getName());
		}
	}
}

