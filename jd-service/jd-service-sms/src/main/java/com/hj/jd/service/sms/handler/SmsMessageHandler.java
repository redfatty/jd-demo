package com.hj.jd.service.sms.handler;

import com.hj.api.sms.ISmsService;
import com.hj.api.sms.pojo.SmsCodeMetaInfo;
import com.hj.api.sms.pojo.SmsRespConst;
import com.hj.api.sms.pojo.SmsResponse;
import com.hj.jd.basic.constant.MQConst;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SmsMessageHandler {

	@Autowired
	private ISmsService smsService;

	@RabbitListener(queues = MQConst.QUEUE_USER_SEND_SMS_CODE)
	private void processUserRegisterWithPhoneMsg(SmsCodeMetaInfo metaInfo) {
		int type = metaInfo.getType();
		String authCode = metaInfo.getCode();
		String phone = metaInfo.getPhone();

		System.out.println("准备将验证码" + authCode + "发送到手机:" + phone);
//		SmsResponse response = smsService.sendSmsCode(phone, authCode);
////		if (SmsRespConst.SUCCESS.equals(response.getRespCode())) {
////			System.out.println("短信验证码发送成功");
////		} else {
////			System.out.println("短信验证码发送失败");
////		}
	}
}
