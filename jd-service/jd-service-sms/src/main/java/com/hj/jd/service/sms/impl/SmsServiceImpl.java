package com.hj.jd.service.sms.impl;

import com.google.gson.Gson;
import com.hj.api.sms.ISmsService;
import com.hj.api.sms.pojo.SmsResponse;
import com.hj.jd.service.sms.common.HttpUtil;
import com.hj.jd.service.sms.common.SmsConfig;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class SmsServiceImpl implements ISmsService {

	private static String operation = "/industrySMS/sendSMS";
	private static String accountSid = SmsConfig.ACCOUNT_SID;

	@Override
	public SmsResponse sendSmsCode(String toPhone, String authCode) {
		StringBuilder stringBuilder = new StringBuilder("【xx科技】您的短信验证码为:");
		stringBuilder.append(authCode);
		stringBuilder.append(", 请在5分钟内使用");
		String encodedContent = null;
		try {
			encodedContent = URLEncoder.encode(stringBuilder.toString(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		String url = SmsConfig.BASE_URL + operation;
		String body = "accountSid=" + accountSid + "&to=" + toPhone + "&smsContent=" + encodedContent
				+ HttpUtil.createCommonParam();

		// 提交请求
		String result = HttpUtil.post(url, body);
		return new Gson().fromJson(result, SmsResponse.class);
	}
}
