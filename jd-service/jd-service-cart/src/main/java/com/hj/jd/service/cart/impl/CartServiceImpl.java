package com.hj.jd.service.cart.impl;

import com.hj.jd.api.cart.CartItem;
import com.hj.jd.api.cart.CartItemVO;
import com.hj.jd.api.cart.ICartService;
import com.hj.jd.basic.constant.RedisConst;
import com.hj.jd.basic.entity.Product;
import com.hj.jd.basic.pojo.ResultBean;
import com.hj.jd.basic.util.RedisUtils;
import com.jd.basic.mapper.ProductMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.TimeUnit;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class CartServiceImpl implements ICartService {

	@Autowired
	private RedisTemplate redisTemplate;

	@Autowired
	private ProductMapper productMapper;

	/**
	 * 添加购物车条目
	 * @param cartKey
	 * @param productId
	 * @param productCount
	 * @return
	 */
	@Override
	public ResultBean addCartItem(String cartKey, Long productId, Integer productCount) {
		redisTemplate.setKeySerializer(new StringRedisSerializer());
		ResultBean resultBean = null;

		//1.获取购物车
		List<CartItem> cart = (List<CartItem>) redisTemplate.opsForValue().get(cartKey);

		//2.还没有,则创建
		if (cart == null) {
			cart = new ArrayList<CartItem>();
			resultBean = addCartItemHandler(productId, productCount, cart);
			resetCartInResis(cartKey, cart);
			return resultBean;
		}

		//3.添加的商品是否已经存在
		for (CartItem item : cart) {
			//注意转成基本数据类型,再比较是否相等
			if (item.getProductId().longValue() == productId.longValue()) {
				cart.remove(item);
				Integer newCount = item.getProductCount() + productCount;
				resultBean = addCartItemHandler(productId, newCount, cart);
				resetCartInResis(cartKey, cart);
				return resultBean;
			}
		}

		//4.新添加的商品
		resultBean = addCartItemHandler(productId, productCount, cart);
		resetCartInResis(cartKey, cart);
		return resultBean;
	}

	/**
	 * 获取购物车
	 * @param cartKey
	 * @return
	 */
	@Override
	public ResultBean getCart(String cartKey) {
		redisTemplate.setKeySerializer(new StringRedisSerializer());
		List<CartItem> cart = (List<CartItem>) redisTemplate.opsForValue().get(cartKey);
		//1.购物车为空
		if (cart == null || cart.isEmpty()) {
			return ResultBean.errorBean("购物车为空");
		}

		//2.购物车中有商品,则重置过期时间
		redisTemplate.expire(cartKey, RedisConst.CART_EXPIRE_DAYS, TimeUnit.DAYS);
		return ResultBean.successBean(cart);
	}

	/**
	 * 获取前端视图购物车
	 * @param cartKey
	 * @return
	 */
	@Override
	public ResultBean getCartVO(String cartKey) {
		//先获取购物车
		ResultBean resultBean = getCart(cartKey);

		//购物车为空
		if (resultBean.getErrno() != ResultBean.RESULT_SUCCESS) {
			return ResultBean.errorBean("购物车为空");
		}

		redisTemplate.setKeySerializer(new StringRedisSerializer());
		//购物车中有商品
		List<CartItem> cart = (List<CartItem>) redisTemplate.opsForValue().get(cartKey);
		List<CartItemVO> cartVO = new ArrayList<>(cart.size());
		for (CartItem item : cart) {
			CartItemVO itemVO = new CartItemVO(getProduct(item.getProductId()), item.getProductCount(), item.getUpdateTime());
			cartVO.add(itemVO);
		}

		//按更新时间排序
		Collections.sort(cartVO, (itemVO1, itemVO2) -> (int) (itemVO2.getUpdateTime().getTime() - itemVO1.getUpdateTime().getTime()));

		return ResultBean.successBean(cartVO);
	}

	@Override
	public ResultBean resetCartItemCount(String cartKey, Long productId, Integer productCount) {
		ResultBean resultBean = getCart(cartKey);
		if (resultBean.getErrno() != ResultBean.RESULT_SUCCESS) {
			return resultBean;
		}

		List<CartItem> cart = (List<CartItem>) resultBean.getData();
		for (CartItem item : cart) {
			if (item.getProductId().longValue() == productId.longValue()) {
				item.setProductCount(productCount);
				resetCartInResis(cartKey, cart);
				return ResultBean.successBean(productCount);
			}
		}

		return ResultBean.errorBean("修改数量失败");
	}

	@Override
	public ResultBean removeCartItem(String cartKey, Long productId) {
		ResultBean resultBean = getCart(cartKey);
		if (resultBean.getErrno() != ResultBean.RESULT_SUCCESS) {
			return resultBean;
		}

		List<CartItem> cart = (List<CartItem>) resultBean.getData();
		for (CartItem item : cart) {
			if (item.getProductId().longValue() == productId.longValue()) {
				cart.remove(item);
				resetCartInResis(cartKey, cart);
				return ResultBean.successBean(0);
			}
		}

		return ResultBean.errorBean("移除失败");
	}

	@Override
	public ResultBean mergeCarts(String anonymousCartKey, String userCartKey) {
		redisTemplate.setKeySerializer(new StringRedisSerializer());
		ResultBean anonymousResultBean = getCart(anonymousCartKey);
		List<CartItem> anonymousCart = (List<CartItem>) anonymousResultBean.getData();
		//1.没有未登录的购物车,只需刷新缓存
		if (anonymousResultBean.getErrno() != ResultBean.RESULT_SUCCESS || anonymousCart == null) {
			redisTemplate.expire(userCartKey, RedisConst.CART_EXPIRE_DAYS, TimeUnit.DAYS);
			return ResultBean.errorBean("不存在未登录的购物车, 无需合并");
		}

		ResultBean userResultBean = getCart(userCartKey);
		List<CartItem> userCart = (List<CartItem>) userResultBean.getData();
		//2.有未登录购物车,但没有登录的购物车
		// 直接将购物车由未登录设为已登录
		//并清空未登录数据
		if (userResultBean.getErrno() != ResultBean.RESULT_SUCCESS || userCart == null) {
			resetCartInResis(userCartKey, anonymousCart);
			redisTemplate.delete(anonymousCartKey);
			return new ResultBean(ResultBean.RESULT_SUCCESS, anonymousCart.size(), "合并成功");
		}

		//3.未登录和已登录的购物车都存在,则合并
		Map<Long, CartItem> cartMap = new HashMap<>();
		for (CartItem item : anonymousCart) {
			cartMap.put(item.getProductId(), item);
		}

		for (CartItem item : userCart) {
			Long productId = item.getProductId();
			CartItem existItem = cartMap.get(productId);
			if (existItem== null) {
				cartMap.put(productId, item);
			} else {
				//数量相加
				existItem.setProductCount(existItem.getProductCount() + item.getProductCount());
			}
		}

		//将map转为list
		List<CartItem> mergedCart = new ArrayList<>(cartMap.values());
		resetCartInResis(userCartKey, mergedCart);
		redisTemplate.delete(anonymousCartKey);

		return new ResultBean(ResultBean.RESULT_SUCCESS, mergedCart.size(), "合并成功");
	}


	private Product getProduct(Long productId) {
		redisTemplate.setKeySerializer(new StringRedisSerializer());
		String productKey = RedisUtils.getProductKey(productId);
		//1.redis缓存中取
		Product product = (Product) redisTemplate.opsForValue().get(productKey);
		if (product != null) {
//			redisTemplate.expire(productKey, RedisConst.PRODUCT_EXPIRE_MINUTES, TimeUnit.MINUTES);
			return product;
		}

		//2.从数据库中取
		String lockKey = RedisUtils.getProductLockKey(productId);
		String lockValue= UUID.randomUUID().toString();
		boolean isAbsent = redisTemplate.opsForValue().setIfAbsent(lockKey, lockValue);
		if (isAbsent) {
			//2.1.此刻没有被其它线程锁住
			try {
				product = productMapper.selectByPrimaryKey(productId);
				redisTemplate.opsForValue().set(productKey, product);
				redisTemplate.expire(productKey, RedisConst.PRODUCT_EXPIRE_MINUTES, TimeUnit.MINUTES);
			} finally {
				//最后删除锁
				if (lockValue.equals(redisTemplate.opsForValue().get(lockKey))) {
					redisTemplate.delete(lockKey);
				}
			}
		} else {
			//2.2.已经被其它线程锁住, 10毫秒后再试
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			getProduct(productId);
		}

		return product;
	}

	/**
	 * 添加购物车条目
	 * 只会创建一个购物车条目对象,添加到cart集合中, 不会更新缓存
	 * @param productId
	 * @param productCount
	 * @param cart
	 * @return
	 */
	private ResultBean addCartItemHandler(Long productId, Integer productCount, List<CartItem> cart) {
		redisTemplate.setKeySerializer(new StringRedisSerializer());
		CartItem item = new CartItem(productId, productCount, new Date());
		cart.add(item);
		return ResultBean.successBean(cart.size());
	}

	/**
	 * 更新购物车在redis中的缓存
	 * @param cartKey
	 * @param cart
	 */
	private void resetCartInResis(String cartKey, Collection<CartItem> cart) {
		redisTemplate.opsForValue().set(cartKey, cart);
		redisTemplate.expire(cartKey, RedisConst.CART_EXPIRE_DAYS, TimeUnit.DAYS);
	}
}
