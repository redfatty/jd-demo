package com.hj.jd.service.order.impl;

import com.hj.jd.api.order.IOrderService;
import com.hj.jd.basic.base.BaseServiceImpl;
import com.hj.jd.basic.base.IBaseDao;
import com.hj.jd.basic.entity.Order;
import com.jd.basic.mapper.OrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class OrderServiceImpl extends BaseServiceImpl<Order> implements IOrderService {

	@Autowired
	private OrderMapper orderMapper;

	@Override
	public IBaseDao<Order> getBaseDao() {
		return orderMapper;
	}
}
