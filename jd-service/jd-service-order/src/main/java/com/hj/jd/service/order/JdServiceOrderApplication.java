package com.hj.jd.service.order;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableDubbo
public class JdServiceOrderApplication {

	public static void main(String[] args) {
		SpringApplication.run(JdServiceOrderApplication.class, args);
	}

}

