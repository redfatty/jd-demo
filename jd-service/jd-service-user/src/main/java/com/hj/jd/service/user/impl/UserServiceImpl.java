package com.hj.jd.service.user.impl;

import com.hj.jd.api.user.IUserService;
import com.hj.jd.basic.constant.RedisConst;
import com.hj.jd.basic.entity.User;
import com.hj.jd.basic.pojo.ResultBean;
import com.hj.jd.basic.util.RedisUtils;
import com.jd.basic.mapper.UserMapper;
import org.apache.ibatis.executor.ReuseExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class UserServiceImpl implements IUserService {

	@Autowired
	private UserMapper userMapper;

	@Autowired
	private RedisTemplate redisTemplate;


	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Override
	public boolean isRegisteredPhone(String phone) {
		System.out.println("判断手机号是否已经注册: " + phone);
		return false;
	}

	@Override
	public User login(String loginname, String password) {
		User user = userMapper.getByLoginname(loginname);
		if (user == null || !passwordEncoder.matches(password, user.getPassword())) {
			//用户名或密码错误
			return null;
		}

		redisTemplate.setKeySerializer(new StringRedisSerializer());

		//将上次登录信息清空
		String tokenKey = RedisUtils.getUserTokenKey(user.getId());
		Object oldToken = redisTemplate.opsForValue().get(tokenKey);
		if (oldToken != null) {
			redisTemplate.delete(tokenKey);
			redisTemplate.delete(RedisUtils.getUserInfoKey((String) oldToken));
		}

		//生成一个新的token
		String newToken = UUID.randomUUID().toString();
		user.setToken(newToken);
		user.setPassword(null);

		//保存当次登录信息
		//保存token
		String userInfoKey = RedisUtils.getUserInfoKey(newToken);
		redisTemplate.opsForValue().set(tokenKey, newToken);
		redisTemplate.expire(tokenKey, RedisConst.USER_EXPIRE_MINUTES, TimeUnit.MINUTES);
		//保存用户信息
		redisTemplate.opsForValue().set(userInfoKey, user);
		redisTemplate.expire(userInfoKey, RedisConst.USER_EXPIRE_MINUTES, TimeUnit.MINUTES);//30分钟

		return user;
	}

	@Override
	public void logout(String token) {
		if (token == null) {
			return;
		}

		redisTemplate.setKeySerializer(new StringRedisSerializer());
		String userInfoKey = RedisUtils.getUserInfoKey(token);
		User user = (User) redisTemplate.opsForValue().get(userInfoKey);
		//删除用户信息
		redisTemplate.delete(userInfoKey);
		if (user != null) {
			//删除token
			redisTemplate.delete(RedisUtils.getUserTokenKey(user.getId()));
		}
	}

	@Override
	public ResultBean checkLogin(String token) {
		redisTemplate.setKeySerializer(new StringRedisSerializer());

		if (token == null) {
			return ResultBean.errorBean("用户未登录~");
		}

		//1.根据uuid从redis获取用户信息
		String userInfoKey = RedisUtils.getUserInfoKey(token);
		User user = (User) redisTemplate.opsForValue().get(userInfoKey);

		//2.
		if (user == null) {
			return ResultBean.errorBean("用户未登录");
		}

		//3.重置过期时间
		redisTemplate.expire(userInfoKey, RedisConst.USER_EXPIRE_MINUTES, TimeUnit.MINUTES);
		redisTemplate.expire(RedisUtils.getUserTokenKey(user.getId()), RedisConst.USER_EXPIRE_MINUTES, TimeUnit.MINUTES);
		return ResultBean.successBean(user);
	}

	@Override
	public Long add(User user) {
		//密码加密
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		return (long) userMapper.insertSelective(user);
	}
}
