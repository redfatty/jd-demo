package com.hj.jd.service.user;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
@MapperScan("com.jd.basic.mapper")
@EnableDubbo
public class JdServiceUserApplication {

	public static void main(String[] args) {
		SpringApplication.run(JdServiceUserApplication.class, args);
	}

}

