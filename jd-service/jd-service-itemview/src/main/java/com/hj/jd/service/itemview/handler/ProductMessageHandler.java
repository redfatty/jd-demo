package com.hj.jd.service.itemview.handler;

import com.hj.jd.api.itemview.IItemViewService;
import com.hj.jd.basic.constant.MQConst;
import com.hj.jd.basic.entity.Product;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProductMessageHandler {

	@Autowired
	private IItemViewService itemViewService;

	@RabbitListener(queues = MQConst.QUEUE_PRODUCT_ADD)
	public void processProductAddingMessage(Product product) {
		itemViewService.createItemView(product);
	}

	public void processProductDeletionMessage(Long productId) {

	}
}
