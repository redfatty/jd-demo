package com.hj.jd.service.itemview.impl;

import com.hj.jd.api.itemview.IItemViewService;
import com.hj.jd.basic.entity.Product;
import com.hj.jd.basic.pojo.ResultBean;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.apache.zookeeper.common.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.WildcardType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

@Service
@com.alibaba.dubbo.config.annotation.Service
public class ItemViewServiceImpl implements IItemViewService {

	private static final Integer CORE_THREADS = Runtime.getRuntime().availableProcessors();
	private static final Integer MAX_THREADS = CORE_THREADS * 2;
	private static ExecutorService threadPool;
	static {
		threadPool = new ThreadPoolExecutor(
				CORE_THREADS,
				MAX_THREADS,
				10L,
				TimeUnit.MILLISECONDS,
				new LinkedBlockingQueue<Runnable>(100));
	}

	@Autowired
	private Configuration configuration;

	@Value("${staticview.path}")
	private String staticViewPath;

	@Override
	public ResultBean createItemView(Product product) {
		boolean successed = processCreatePage(product);
		if (successed) {
			return ResultBean.successBean("创建视图成功, product_id:" + product.getId());
		} else {
			return ResultBean.errorBean("创建视图失败, product_id:" + product.getId());
		}
	}

	@Override
	public ResultBean batchCreateItemView(List<Product> products) {
		if (products == null || products.size() < 1) {
			return ResultBean.errorBean("至少要有一个商品");
		}

//		List<Future<Boolean>> futures = new ArrayList<>(products.size());
		List<Boolean> results = new ArrayList<>(products.size());
		for (Product product : products) {
			//将每次创建任务提交到线程池中
			Future<Boolean> future = threadPool.submit(()->{
				return processCreatePage(product);
			});
//			futures.add(future);
			try {
				results.add(future.get());
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}

		//阻塞至所有任务都完成后, 才往下执行
		return ResultBean.successBean("批量创建视图成功");
	}

	private boolean processCreatePage(Product product) {
		boolean successed = false;
		FileWriter writer = null;
		try {
			Template template = configuration.getTemplate("itemview.ftl");
			Map<String, Product> datas = new HashMap<>();
			datas.put("product", product);
			String dest = staticViewPath + product.getId() + ".html";
			writer = new FileWriter(dest);
			template.process(datas, writer);
			successed = true;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TemplateException e) {
			e.printStackTrace();
		} finally {
			IOUtils.closeStream(writer);
			return successed;
		}
	}
}
