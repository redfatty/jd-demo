package com.hj.jd.service.itemview;

import com.alibaba.dubbo.config.annotation.Reference;
import com.hj.jd.api.itemview.IItemViewService;
import com.hj.jd.basic.entity.Product;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JdServiceItemviewApplicationTests {

	@Reference
	private IItemViewService itemViewService;

	@Test
	public void contextLoads() {
		Product product = new Product();
		product.setId(88L);
		product.setName("诺基亚");

		itemViewService.createItemView(product);
	}

}

