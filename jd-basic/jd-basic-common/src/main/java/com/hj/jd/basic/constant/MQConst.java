package com.hj.jd.basic.constant;

public interface MQConst {


	/**
	 * 商品
	 */
	String EXCHANGE_PRODUCT = "exchange_product";

	String QUEUE_PRODUCT_ADD = "queue_produc_add";
	String QUEUE_PRODUCT_DELETE = "queue_produc_delete";

	String ROUTINGKEY_PRODUCT_ADD = "product.add";
	String ROUTINGKEY_PRODUCT_DELETE = "product.delete";

	/**
	 * 用户
	 */
	String EXCHANGE_USER = "exchange_user";
	String QUEUE_USER_REGISTER_EMAIL = "queue_user_register_email"; //邮箱注册
	String QUEUE_USER_SEND_SMS_CODE = "queue_user_send_sms_code"; //发送短信验证码
	String ROUTINGKEY_USER_REGISTER_EMAIL = "user.register.eamil";
	String ROUTINGKEY_USER_SEND_SMS_CODE = "user.send.smscode";
}
