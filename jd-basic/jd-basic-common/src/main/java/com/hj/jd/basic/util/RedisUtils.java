package com.hj.jd.basic.util;

import com.hj.jd.basic.constant.RedisConst;

public class RedisUtils {
	public static String getUserInfoKey(String token) {
		return new StringBuilder(RedisConst.USER_INFO_PREFIX).append(token).toString();
	}

	public static String getUserTokenKey(Long userId) {
		return new StringBuilder("user:").append(userId).append(":token").toString();
	}

	// 匿名购物车的key
	public static String getAnonymousCartKey(String cartUuid) {
		return new StringBuilder(RedisConst.USER_CART_PREFIX).append(cartUuid).toString();
	}

	// 登录用户购物车的key
	public static String getUserCartKey(Long userId) {
		return new StringBuilder(RedisConst.USER_CART_PREFIX).append(userId).toString();
	}

	public static String getProductKey(Long productId) {
		return new StringBuilder(RedisConst.PRODUCT_PREFIX).append(productId).toString();
	}

	public static String getProductLockKey(Long productId) {
		return new StringBuilder("product:").append(productId).append(":lock").toString();
	}
}
