package com.hj.jd.basic.constant;

public interface CookieConst {
	String USER_TOKEN = "user_token";
	String USER_CART_UUID = "user_cart_uuid";
	String PARENT_DOMAIN = "jdd.com";
}
