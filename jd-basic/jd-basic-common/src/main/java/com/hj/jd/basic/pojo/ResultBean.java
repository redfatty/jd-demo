package com.hj.jd.basic.pojo;

import java.io.Serializable;

public class ResultBean implements Serializable {
	public static final Integer RESULT_SUCCESS = 0;
	public static final Integer RESULT_ERROR = 1;
	public static final Integer RESULT_LOGIN_HACKED = 2;

	private Integer errno;
	private Object data;
	private String message;

	public static ResultBean errorBean(String message) {
		ResultBean errorBean = new ResultBean();
		errorBean.setErrno(RESULT_ERROR);
		errorBean.setMessage(message);
		errorBean.setData("");
		return  errorBean;
	}

	public static ResultBean successBean(Object data) {
		ResultBean successBean = new ResultBean();
		successBean.setErrno(RESULT_SUCCESS);
		successBean.setMessage("");
		successBean.setData(data);
		return  successBean;
	}

	public ResultBean(Integer errno, Object data, String message) {
		this.errno = errno;
		this.data = data;
		this.message = message;
	}

	public ResultBean() {
	}

	public Integer getErrno() {
		return errno;
	}

	public void setErrno(Integer errno) {
		this.errno = errno;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
