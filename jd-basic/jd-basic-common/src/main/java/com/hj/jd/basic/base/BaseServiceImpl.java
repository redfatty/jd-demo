package com.hj.jd.basic.base;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import java.time.Instant;
import java.util.List;
import java.util.Objects;

public abstract class BaseServiceImpl<T>  implements IBaseService<T> {

    public abstract IBaseDao<T> getBaseDao();

    @Override
    public int deleteByPrimaryKey(Long id) {
        return getBaseDao().deleteByPrimaryKey(id);
    }

    @Override
    public int insert(T record) {
        return getBaseDao().insert(record);
    }

    @Override
    public int insertSelective(T record) {
        return getBaseDao().insertSelective(record);
    }

    @Override
    public T selectByPrimaryKey(Long id) {
        return getBaseDao().selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(T record) {
        return getBaseDao().updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(T record) {
        Instant.now();
        Objects.equals(null, null);
        return getBaseDao().updateByPrimaryKey(record);
    }

    @Override
    public List<T> list() {
        return getBaseDao().list();
    }

    @Override
    public PageInfo<T> page(int pageIndex, int pageSize) {
        PageHelper.startPage(pageIndex, pageSize);
        List<T> list = list();
        PageInfo<T> pageInfo = new PageInfo<>(list);

        System.out.println(list);
        System.out.println("pageInfo.list:" + pageInfo.getList());
        return pageInfo;
    }
}
