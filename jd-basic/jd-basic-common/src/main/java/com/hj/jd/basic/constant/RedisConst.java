package com.hj.jd.basic.constant;

public interface RedisConst {
	String USER_INFO_PREFIX = "user:info:";
	String USER_CART_PREFIX = "user:cart:";
	String PRODUCT_PREFIX = "product:";

	long USER_EXPIRE_MINUTES = 30L;
	long CART_EXPIRE_DAYS = 30L;
	long PRODUCT_EXPIRE_MINUTES = 30L;
}
