package com.jd.basic.mapper;

import com.hj.jd.basic.base.IBaseDao;
import com.hj.jd.basic.entity.Order;

public interface OrderMapper extends IBaseDao<Order> {
}
