package com.jd.basic.mapper;

import com.hj.jd.basic.base.IBaseDao;
import com.hj.jd.basic.entity.ProductType;

public interface ProductTypeMapper extends IBaseDao<ProductType> {

}