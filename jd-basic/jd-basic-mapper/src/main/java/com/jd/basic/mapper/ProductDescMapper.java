package com.jd.basic.mapper;


import com.hj.jd.basic.base.IBaseDao;
import com.hj.jd.basic.entity.ProductDesc;

public interface ProductDescMapper extends IBaseDao<ProductDesc> {

}