package com.jd.basic.mapper;


import com.hj.jd.basic.base.IBaseDao;
import com.hj.jd.basic.entity.Product;

import java.util.List;

public interface ProductMapper extends IBaseDao<Product> {

    List<Product> list();
}