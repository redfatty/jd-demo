package com.jd.basic.mapper;

import com.hj.jd.basic.base.IBaseDao;
import com.hj.jd.basic.entity.User;
import org.apache.ibatis.annotations.Param;

public interface UserMapper extends IBaseDao<User> {
//    User login(@Param("loginname") String loginname, @Param("password")String password);

    User getByLoginname(@Param("loginname") String loginname);
}