package com.hj.jd.basic.utils.web;

import com.hj.jd.basic.constant.CookieConst;
import com.hj.jd.basic.constant.DomainConst;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

public class CookieUtils {
	public static void addUserTokenCookie(HttpServletResponse response, String token) {
		Cookie cookie = new Cookie(CookieConst.USER_TOKEN, token);
		cookie.setHttpOnly(true);
		cookie.setPath("/");
		cookie.setDomain(CookieConst.PARENT_DOMAIN);
		response.addCookie(cookie);
	}

	public static void deleteUserTokenCookie(HttpServletResponse response, String token) {
		Cookie cookie = new Cookie(CookieConst.USER_TOKEN, token);
		cookie.setHttpOnly(true);
		cookie.setPath("/");
		cookie.setDomain(CookieConst.PARENT_DOMAIN);
		cookie.setMaxAge(0);
		response.addCookie(cookie);
	}
}
