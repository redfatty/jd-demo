package com.hj.jd.basic.entity;

import java.io.Serializable;

public class EmailTodo implements Serializable {
	private Long id;
	private String toUser;
	private String subject;
	private String content;
	private Integer count;//已经发送的次数

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getToUser() {
		return toUser;
	}

	public void setToUser(String toUser) {
		this.toUser = toUser;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "EmailTodo{" +
				"id=" + id +
				", toUser='" + toUser + '\'' +
				", subject='" + subject + '\'' +
				", content='" + content + '\'' +
				", count=" + count +
				'}';
	}
}
