package com.hj.jd.miaosha;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@MapperScan("com.hj.jd.miaosha.mapper")
@EnableCaching //开启声明式的缓存, 通过注解来控制缓存
@EnableScheduling
public class JdMiaoshaApplication {

	public static void main(String[] args) {
		SpringApplication.run(JdMiaoshaApplication.class, args);
	}

}

