package com.hj.jd.miaosha.service;

import com.hj.jd.miaosha.entity.Product;

public interface IProductService {
	Product getById(Long productId);
}
