package com.hj.jd.miaosha.controller;

import com.hj.jd.miaosha.entity.Product;
import com.hj.jd.miaosha.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/product")
public class ProductController {

	@Autowired
	private IProductService productService;

	@RequestMapping("/getById")
	public String getById(Long id, Model model) {
		System.out.println(id);
		Product product = productService.getById(id);
		model.addAttribute("product", product);
		return "itemview";
	}

}
