package com.hj.jd.miaosha.service.impl;

import com.hj.jd.miaosha.entity.Product;
import com.hj.jd.miaosha.mapper.ProductMapper;
import com.hj.jd.miaosha.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements IProductService {

	@Autowired
	private ProductMapper productMapper;

//	@Autowired
//	private RedisTemplate<String, Object> redisTemplate;

	@Override
//	@Cacheable(key = "#id", value = "product") // 以product::id为key, 在redis中进行存取数据
	public Product getById(Long id) {
		return productMapper.selectByPrimaryKey(id);
	}
}
